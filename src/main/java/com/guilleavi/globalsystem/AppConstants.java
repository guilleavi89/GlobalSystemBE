package com.guilleavi.globalsystem;

public class AppConstants {

	public static final String NAME_SPACE = "com.guilleavi.globalsystem";
	public static final String CONTAINER_NAME = "GlobalSystem";
	public static final String SERVICE_ROOT = "http://localhost:8080/odata/globalsystem.svc/";
	public static final String DEFAULT_EDM_PACKAGE = "com.guilleavi.globalsystem.edm";

	public static final String REGEX_NUMERICO = "(^-?[0-9]*)";
}
