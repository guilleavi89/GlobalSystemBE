package com.guilleavi.globalsystem.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.globalsystem.entities.PersonaEntity;

public interface PersonaRepository
		extends JpaRepository<PersonaEntity, Integer>, QueryDslPredicateExecutor<PersonaEntity> {

}
