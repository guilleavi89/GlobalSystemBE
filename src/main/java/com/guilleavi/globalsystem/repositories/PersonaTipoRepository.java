package com.guilleavi.globalsystem.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.globalsystem.entities.PersonaTipoEntity;

public interface PersonaTipoRepository
		extends JpaRepository<PersonaTipoEntity, Integer>, QueryDslPredicateExecutor<PersonaTipoEntity> {

}
