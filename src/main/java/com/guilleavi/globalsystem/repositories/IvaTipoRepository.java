package com.guilleavi.globalsystem.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.globalsystem.entities.IvaTipoEntity;

public interface IvaTipoRepository
		extends JpaRepository<IvaTipoEntity, Integer>, QueryDslPredicateExecutor<IvaTipoEntity> {

}
