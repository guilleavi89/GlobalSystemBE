package com.guilleavi.globalsystem.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.globalsystem.entities.CiudadEntity;

public interface CiudadRepository
		extends JpaRepository<CiudadEntity, Integer>, QueryDslPredicateExecutor<CiudadEntity> {

}
