package com.guilleavi.globalsystem.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.globalsystem.entities.ClienteEntity;

public interface ClienteRepository
		extends JpaRepository<ClienteEntity, Integer>, QueryDslPredicateExecutor<ClienteEntity> {

}
