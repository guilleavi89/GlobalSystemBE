package com.guilleavi.globalsystem.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.globalsystem.entities.ProvinciaEntity;

public interface ProvinciaRepository
		extends JpaRepository<ProvinciaEntity, Integer>, QueryDslPredicateExecutor<ProvinciaEntity> {

}
