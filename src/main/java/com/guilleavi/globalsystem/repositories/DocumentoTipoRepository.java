package com.guilleavi.globalsystem.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;

import com.guilleavi.globalsystem.entities.DocumentoTipoEntity;

public interface DocumentoTipoRepository
		extends JpaRepository<DocumentoTipoEntity, Integer>, QueryDslPredicateExecutor<DocumentoTipoEntity> {

}
