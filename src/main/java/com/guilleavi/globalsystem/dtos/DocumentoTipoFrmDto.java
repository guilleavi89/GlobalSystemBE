package com.guilleavi.globalsystem.dtos;

import com.guilleavi.globalsystem.edm.resources.DocumentoTipoEdm;

public class DocumentoTipoFrmDto {

	private Integer id = null;
	private String descripcion = null;
	private String descripcionReducida = null;

	public DocumentoTipoFrmDto() {
		super();
	}

	public DocumentoTipoFrmDto(Integer id, String descripcion, String descripcionReducida) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.descripcionReducida = descripcionReducida;
	}

	public DocumentoTipoFrmDto(DocumentoTipoEdm documentoTipoEdm) {
		this(documentoTipoEdm.getId(),
				documentoTipoEdm.getDescripcion() == null || documentoTipoEdm.getDescripcion().trim().isEmpty() ? null
						: documentoTipoEdm.getDescripcion().trim().toUpperCase(),
				documentoTipoEdm.getDescripcionReducida() == null
						|| documentoTipoEdm.getDescripcionReducida().trim().isEmpty() ? null
								: documentoTipoEdm.getDescripcionReducida().trim().toUpperCase());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionReducida() {
		return descripcionReducida;
	}

	public void setDescripcionReducida(String descripcionReducida) {
		this.descripcionReducida = descripcionReducida;
	}

}
