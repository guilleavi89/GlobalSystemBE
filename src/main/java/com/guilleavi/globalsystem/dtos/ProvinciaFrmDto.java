package com.guilleavi.globalsystem.dtos;

import com.guilleavi.globalsystem.edm.resources.ProvinciaEdm;

public class ProvinciaFrmDto {
	private Integer id = null;
	private String nombre = null;
	private Integer paisId = null;

	public ProvinciaFrmDto() {
		super();
	}

	public ProvinciaFrmDto(Integer id, String nombre, Integer paisId) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.paisId = paisId;
	}

	public ProvinciaFrmDto(ProvinciaEdm provinciaEdm) {
		this(provinciaEdm.getId(), 
				provinciaEdm.getNombre() == null || provinciaEdm.getNombre().trim().isEmpty() ? null
						: provinciaEdm.getNombre().trim().toUpperCase(), 
				provinciaEdm.getPais().getId());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getPaisId() {
		return paisId;
	}

	public void setPaisId(Integer paisId) {
		this.paisId = paisId;
	}

}
