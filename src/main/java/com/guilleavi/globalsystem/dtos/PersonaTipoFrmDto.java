package com.guilleavi.globalsystem.dtos;

import com.guilleavi.globalsystem.edm.resources.PersonaTipoEdm;

public class PersonaTipoFrmDto {

	private Integer id = null;
	private String descripcion = null;
	

	public PersonaTipoFrmDto() {
		super();
	}

	public PersonaTipoFrmDto(Integer id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public PersonaTipoFrmDto(PersonaTipoEdm personaTipoEdm) {
		this(personaTipoEdm.getId(), 
				personaTipoEdm.getDescripcion() == null || personaTipoEdm.getDescripcion().trim().isEmpty() ? null
						: personaTipoEdm.getDescripcion().trim().toUpperCase());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
