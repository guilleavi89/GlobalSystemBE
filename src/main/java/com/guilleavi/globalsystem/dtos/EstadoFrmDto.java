package com.guilleavi.globalsystem.dtos;

import com.guilleavi.globalsystem.edm.resources.EstadoEdm;

public class EstadoFrmDto {

	private Integer id = null;
	private String descripcion = null;

	public EstadoFrmDto() {
		super();
	}

	public EstadoFrmDto(Integer id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public EstadoFrmDto(EstadoEdm estadoEdm) {
		this(estadoEdm.getId(), 
				estadoEdm.getDescripcion() == null || estadoEdm.getDescripcion().trim().isEmpty() ? null
						: estadoEdm.getDescripcion().trim().toUpperCase());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

}
