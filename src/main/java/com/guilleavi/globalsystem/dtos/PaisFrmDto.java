package com.guilleavi.globalsystem.dtos;

import com.guilleavi.globalsystem.edm.resources.PaisEdm;

public class PaisFrmDto {

	private Integer id = null;
	private String nombre = null;
	private String prefijoTelefonico = null;

	public PaisFrmDto() {
		super();
	}

	public PaisFrmDto(Integer id, String nombre, String prefijoTelefonico) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.prefijoTelefonico = prefijoTelefonico;
	}

	public PaisFrmDto(PaisEdm paisEdm) {
		this(paisEdm.getId(), 
				paisEdm.getNombre() == null || paisEdm.getNombre().trim().isEmpty() ? null
						: paisEdm.getNombre().trim().toUpperCase(), 
				paisEdm.getPrefijoTelefonico() == null || paisEdm.getPrefijoTelefonico().trim().isEmpty() ? null
						: paisEdm.getPrefijoTelefonico().trim().toUpperCase());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrefijoTelefonico() {
		return prefijoTelefonico;
	}

	public void setPrefijoTelefonico(String prefijoTelefonico) {
		this.prefijoTelefonico = prefijoTelefonico;
	}

}
