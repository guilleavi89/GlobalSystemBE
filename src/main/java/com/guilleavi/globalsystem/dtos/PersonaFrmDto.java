package com.guilleavi.globalsystem.dtos;

import com.guilleavi.globalsystem.edm.resources.PersonaEdm;

public class PersonaFrmDto {
	private Integer id = null;
	private String nombre = null;
	private Integer tipoId = null;
	private Integer documentoTipoId = null;
	private Long documentoNumero = null;

	public PersonaFrmDto() {
		super();
	}

	public PersonaFrmDto(Integer id, String nombre, Integer tipoId, Integer documentoTipoId, Long documentoNumero) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.tipoId = tipoId;
		this.documentoTipoId = documentoTipoId;
		this.documentoNumero = documentoNumero;
	}

	public PersonaFrmDto(PersonaEdm personaEdm) {
		this(personaEdm.getId(), 
				personaEdm.getNombre() == null || personaEdm.getNombre().trim().isEmpty() ? null
						: personaEdm.getNombre().trim().toUpperCase(), 
				personaEdm.getTipo().getId(),
				personaEdm.getDocumentoTipo().getId(), 
				personaEdm.getDocumentoNumero());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getTipoId() {
		return tipoId;
	}

	public void setTipoId(Integer tipoId) {
		this.tipoId = tipoId;
	}

	public Integer getDocumentoTipoId() {
		return documentoTipoId;
	}

	public void setDocumentoTipoId(Integer documentoTipoId) {
		this.documentoTipoId = documentoTipoId;
	}

	public Long getDocumentoNumero() {
		return documentoNumero;
	}

	public void setDocumentoNumero(Long documentoNumero) {
		this.documentoNumero = documentoNumero;
	}

}
