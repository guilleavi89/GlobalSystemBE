package com.guilleavi.globalsystem.dtos;

import com.guilleavi.globalsystem.edm.resources.CiudadEdm;

public class CiudadFrmDto {

	private Integer id = null;
	private String nombre = null;
	private String codigoPostal = null;
	private Integer provinciaId = null;
	private String prefijoTelefonico = null;

	public CiudadFrmDto() {
		super();
	}

	public CiudadFrmDto(Integer id, String nombre, String codigoPostal, Integer provinciaId, String prefijoTelefonico) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.codigoPostal = codigoPostal;
		this.provinciaId = provinciaId;
		this.prefijoTelefonico = prefijoTelefonico;
	}

	public CiudadFrmDto(CiudadEdm ciudadEdm) {
		this(ciudadEdm.getId(),
				ciudadEdm.getNombre() == null || ciudadEdm.getNombre().trim().isEmpty() ? null
						: ciudadEdm.getNombre().trim().toUpperCase(),
				ciudadEdm.getCodigoPostal() == null || ciudadEdm.getCodigoPostal().trim().isEmpty() ? null
						: ciudadEdm.getCodigoPostal().trim().toUpperCase(),
				ciudadEdm.getProvincia().getId(),
				ciudadEdm.getPrefijoTelefonico() == null || ciudadEdm.getPrefijoTelefonico().trim().isEmpty() ? null
						: ciudadEdm.getPrefijoTelefonico().trim().toUpperCase());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public Integer getProvinciaId() {
		return provinciaId;
	}

	public void setProvinciaId(Integer provinciaId) {
		this.provinciaId = provinciaId;
	}

	public String getPrefijoTelefonico() {
		return prefijoTelefonico;
	}

	public void setPrefijoTelefonico(String prefijoTelefonico) {
		this.prefijoTelefonico = prefijoTelefonico;
	}

}
