package com.guilleavi.globalsystem.dtos;

import com.guilleavi.globalsystem.edm.resources.IvaTipoEdm;

public class IvaTipoFrmDto {

	private Integer id = null;
	private String descripcion = null;
	

	public IvaTipoFrmDto() {
		super();
	}

	public IvaTipoFrmDto(Integer id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public IvaTipoFrmDto(IvaTipoEdm ivaTipoEdm) {
		this(ivaTipoEdm.getId(), 
				ivaTipoEdm.getDescripcion() == null || ivaTipoEdm.getDescripcion().trim().isEmpty() ? null
						: ivaTipoEdm.getDescripcion().trim().toUpperCase());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}
}
