package com.guilleavi.globalsystem.dtos;

import com.guilleavi.globalsystem.edm.resources.DomicilioEdm;

public class DomicilioFrmDto {

	private Integer id = null;
	private Integer personaId = null;
	private String calle = null;
	private String nro = null;
	private String piso = null;
	private String dpto = null;
	private Integer ciudadId = null;
	private String descripcion = null;
	private Boolean principal = null;

	public DomicilioFrmDto() {
		super();
	}

	public DomicilioFrmDto(Integer id, Integer personaId, String calle, String nro, String piso, String dpto,
			Integer ciudadId, String descripcion, Boolean principal) {
		super();
		this.id = id;
		this.personaId = personaId;
		this.calle = calle;
		this.nro = nro;
		this.piso = piso;
		this.dpto = dpto;
		this.ciudadId = ciudadId;
		this.descripcion = descripcion;
		this.principal = principal;
	}

	public DomicilioFrmDto(DomicilioEdm domicilioEdm) {
		this(domicilioEdm.getId(), domicilioEdm.getPersona().getId(),
				domicilioEdm.getCalle() == null || domicilioEdm.getCalle().trim().isEmpty() ? null
						: domicilioEdm.getCalle().trim().toUpperCase(),
				domicilioEdm.getNro() == null || domicilioEdm.getNro().trim().isEmpty() ? null
						: domicilioEdm.getNro().trim().toUpperCase(),
				domicilioEdm.getPiso() == null || domicilioEdm.getPiso().trim().isEmpty() ? null
						: domicilioEdm.getPiso().trim().toUpperCase(),
				domicilioEdm.getDpto() == null || domicilioEdm.getDpto().trim().isEmpty() ? null
						: domicilioEdm.getDpto().trim().toUpperCase(),
				domicilioEdm.getCiudad().getId(),
				domicilioEdm.getDescripcion() == null || domicilioEdm.getDescripcion().trim().isEmpty() ? null
						: domicilioEdm.getDescripcion().trim().toUpperCase(),
				domicilioEdm.getPrincipal());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPersonaId() {
		return personaId;
	}

	public void setPersonaId(Integer personaId) {
		this.personaId = personaId;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNro() {
		return nro;
	}

	public void setNro(String nro) {
		this.nro = nro;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}

	public Integer getCiudadId() {
		return ciudadId;
	}

	public void setCiudadId(Integer ciudadId) {
		this.ciudadId = ciudadId;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getPrincipal() {
		return principal;
	}

	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}

}
