package com.guilleavi.globalsystem.dtos.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.guilleavi.globalsystem.dtos.DocumentoTipoFrmDto;

@Component
public class DocumentoTipoFrmDtoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return (DocumentoTipoFrmDto.class).isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		ValidationUtils.rejectIfEmpty(errors, "descripcion", "required", new Object[] { "'Descripción'" });
		ValidationUtils.rejectIfEmpty(errors, "descripcionReducida", "required",
				new Object[] { "'Descripción Reducida'" });
	}

}
