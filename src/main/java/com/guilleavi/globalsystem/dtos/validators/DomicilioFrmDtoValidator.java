package com.guilleavi.globalsystem.dtos.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.guilleavi.globalsystem.dtos.DomicilioFrmDto;

@Component
public class DomicilioFrmDtoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return (DomicilioFrmDto.class).isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		ValidationUtils.rejectIfEmpty(errors, "personaId", "required", new Object[] { "'Id Persona'" });
		ValidationUtils.rejectIfEmpty(errors, "calle", "required", new Object[] { "'Calle'" });
		ValidationUtils.rejectIfEmpty(errors, "nro", "required", new Object[] { "'Nro'" });
		ValidationUtils.rejectIfEmpty(errors, "ciudadId", "required", new Object[] { "'Id Ciudad'" });
		ValidationUtils.rejectIfEmpty(errors, "principal", "required", new Object[] { "'Principal'" });
	}

}
