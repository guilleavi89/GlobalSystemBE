package com.guilleavi.globalsystem.dtos.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.guilleavi.globalsystem.dtos.PersonFrmDto;

@Component
public class PersonFrmDtoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return (PersonFrmDto.class).isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		ValidationUtils.rejectIfEmpty(errors, "lastname", "required", new Object[] {"'lastname'"});
		ValidationUtils.rejectIfEmpty(errors, "name", "required", new Object[] {"'name'"});
	}
}
