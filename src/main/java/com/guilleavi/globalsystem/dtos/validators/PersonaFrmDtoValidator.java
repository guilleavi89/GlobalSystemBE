package com.guilleavi.globalsystem.dtos.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.guilleavi.globalsystem.dtos.PersonaFrmDto;

@Component
public class PersonaFrmDtoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return (PersonaFrmDto.class).isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		ValidationUtils.rejectIfEmpty(errors, "nombre", "required", new Object[] { "'Nombre'" });
		ValidationUtils.rejectIfEmpty(errors, "tipoId", "required", new Object[] { "'Id Tipo'" });
	}

}
