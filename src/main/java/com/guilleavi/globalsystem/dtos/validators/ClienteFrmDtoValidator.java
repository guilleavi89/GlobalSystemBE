package com.guilleavi.globalsystem.dtos.validators;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.guilleavi.globalsystem.dtos.ClienteFrmDto;

@Component
public class ClienteFrmDtoValidator implements Validator {

	@Override
	public boolean supports(Class<?> clazz) {
		return (ClienteFrmDto.class).isAssignableFrom(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {

		ValidationUtils.rejectIfEmpty(errors, "personaId", "required", new Object[] { "'Id Persona'" });
		ValidationUtils.rejectIfEmpty(errors, "ivaTipoId", "required", new Object[] { "'Id Tipo de Iva'" });
		ValidationUtils.rejectIfEmpty(errors, "estadoId", "required", new Object[] { "'Id Estado'" });
	}
}
