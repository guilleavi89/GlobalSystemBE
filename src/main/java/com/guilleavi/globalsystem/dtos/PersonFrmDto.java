package com.guilleavi.globalsystem.dtos;

import com.guilleavi.globalsystem.edm.resources.PersonEdm;

public class PersonFrmDto {

	private Integer id = null;
	private String lastname = null;
	private String name = null;

	public PersonFrmDto() {
		super();
	}

	public PersonFrmDto(Integer id, String lastname, String name) {
		super();
		this.id = id;
		this.lastname = lastname;
		this.name = name;
	}

	public PersonFrmDto(PersonEdm personEdm) {
		this(personEdm.getId(), personEdm.getLastname(), personEdm.getName());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}
}
