package com.guilleavi.globalsystem.dtos;

import com.guilleavi.globalsystem.edm.resources.ClienteEdm;

public class ClienteFrmDto {

	private Integer id = null;
	private Integer personaId = null;
	private Integer ivaTipoId = null;
	private Integer estadoId = null;

	public ClienteFrmDto() {
		super();
	}

	public ClienteFrmDto(Integer id, Integer personaId, Integer ivaTipoId, Integer estadoId) {
		super();
		this.id = id;
		this.personaId = personaId;
		this.ivaTipoId = ivaTipoId;
		this.estadoId = estadoId;
	}

	public ClienteFrmDto(ClienteEdm clienteEdm) {
		this(clienteEdm.getId(), clienteEdm.getPersona().getId(), clienteEdm.getIvaTipo().getId(),
				clienteEdm.getEstado().getId());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getPersonaId() {
		return personaId;
	}

	public void setPersonaId(Integer personaId) {
		this.personaId = personaId;
	}

	public Integer getIvaTipoId() {
		return ivaTipoId;
	}

	public void setIvaTipoId(Integer ivaTipoId) {
		this.ivaTipoId = ivaTipoId;
	}

	public Integer getEstadoId() {
		return estadoId;
	}

	public void setEstadoId(Integer estadoId) {
		this.estadoId = estadoId;
	}
}
