package com.guilleavi.globalsystem.utils;

public class NumbersUtil {
	
	public static boolean isNumeric(String s) {
		return s.matches("\\d+");
	}
	
}
