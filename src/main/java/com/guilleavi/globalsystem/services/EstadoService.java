package com.guilleavi.globalsystem.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.globalsystem.dtos.EstadoFrmDto;
import com.guilleavi.globalsystem.entities.EstadoEntity;
import com.guilleavi.globalsystem.exceptions.ServiceException;
import com.guilleavi.globalsystem.repositories.EstadoRepository;

@Service
public class EstadoService {

	@Autowired
	private EstadoRepository estadoRepository = null;

	@Transactional(readOnly = true)
	public Boolean exists(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de estado no puede ser nulo.");
		}

		EstadoEntity estadoEntity = estadoRepository.findOne(id);

		if (estadoEntity == null) {
			return false;
		} else {
			return true;
		}

	}

	@Transactional(readOnly = true)
	public EstadoEntity findById(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de estado no puede ser nulo.");
		}

		EstadoEntity estadoEntity = estadoRepository.findOne(id);

		if (estadoEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra un estado con id = %s.", id));
		}

		return estadoEntity;
	}

	@Transactional(readOnly = true)
	public List<EstadoEntity> findEstados() {

		List<EstadoEntity> estados = estadoRepository.findAll();

		return estados;
	}

	@Transactional
	public EstadoEntity update(EstadoFrmDto estadoFrmDto) throws ServiceException {

		EstadoEntity estadoEntity = new EstadoEntity();

		estadoEntity.setId(estadoFrmDto.getId());
		estadoEntity.setDescripcion(estadoFrmDto.getDescripcion());
		estadoRepository.save(estadoEntity);

		return estadoEntity;

	}

	@Transactional
	public void delete(EstadoEntity estadoEntity) {
		estadoRepository.delete(estadoEntity);
	}
}
