package com.guilleavi.globalsystem.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.globalsystem.dtos.ProvinciaFrmDto;
import com.guilleavi.globalsystem.entities.PaisEntity;
import com.guilleavi.globalsystem.entities.ProvinciaEntity;
import com.guilleavi.globalsystem.exceptions.ServiceException;
import com.guilleavi.globalsystem.repositories.PaisRepository;
import com.guilleavi.globalsystem.repositories.ProvinciaRepository;

@Service
public class ProvinciaService {

	@Autowired
	private ProvinciaRepository provinciaRepository = null;
	@Autowired
	private PaisRepository paisRepository = null;

	@Transactional(readOnly = true)
	public Boolean exists(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de provincia no puede ser nulo.");
		}

		ProvinciaEntity provinciaEntity = provinciaRepository.findOne(id);

		if (provinciaEntity == null) {
			return false;
		} else {
			return true;
		}

	}

	@Transactional(readOnly = true)
	public ProvinciaEntity findById(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de provincia no puede ser nulo.");
		}

		ProvinciaEntity provinciaEntity = provinciaRepository.findOne(id);

		if (provinciaEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra una provincia con id = %s.", id));
		}

		return provinciaEntity;
	}

	@Transactional(readOnly = true)
	public List<ProvinciaEntity> findProvincias() {

		List<ProvinciaEntity> provincias = provinciaRepository.findAll();

		return provincias;
	}

	@Transactional
	public ProvinciaEntity update(ProvinciaFrmDto provinciaFrmDto) throws ServiceException {

		PaisEntity paisEntity = paisRepository.findOne(provinciaFrmDto.getPaisId());

		if (paisEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra un pais con id = %s.", provinciaFrmDto.getPaisId()));
		}
		
		ProvinciaEntity provinciaEntity = new ProvinciaEntity();

		provinciaEntity.setId(provinciaFrmDto.getId());
		provinciaEntity.setNombre(provinciaFrmDto.getNombre());
		provinciaEntity.setPais(paisEntity);
		provinciaRepository.save(provinciaEntity);

		return provinciaEntity;

	}

	@Transactional
	public void delete(ProvinciaEntity provinciaEntity) {
		provinciaRepository.delete(provinciaEntity);
	}
}
