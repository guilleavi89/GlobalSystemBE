package com.guilleavi.globalsystem.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.globalsystem.dtos.DomicilioFrmDto;
import com.guilleavi.globalsystem.entities.CiudadEntity;
import com.guilleavi.globalsystem.entities.DomicilioEntity;
import com.guilleavi.globalsystem.entities.PersonaEntity;
import com.guilleavi.globalsystem.exceptions.ServiceException;
import com.guilleavi.globalsystem.repositories.CiudadRepository;
import com.guilleavi.globalsystem.repositories.DomicilioRepository;
import com.guilleavi.globalsystem.repositories.PersonaRepository;

@Service
public class DomicilioService {

	@Autowired
	private DomicilioRepository domicilioRepository = null;
	@Autowired
	private PersonaRepository personaRepository = null;
	@Autowired
	private CiudadRepository ciudadRepository = null;

	@Transactional(readOnly = true)
	public Boolean exists(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de domicilio no puede ser nulo.");
		}

		DomicilioEntity domicilioEntity = domicilioRepository.findOne(id);

		if (domicilioEntity == null) {
			return false;
		} else {
			return true;
		}

	}

	@Transactional(readOnly = true)
	public DomicilioEntity findById(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de domicilio no puede ser nulo.");
		}

		DomicilioEntity domicilioEntity = domicilioRepository.findOne(id);

		if (domicilioEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra un domicilio con id = %s.", id));
		}

		return domicilioEntity;
	}

	@Transactional(readOnly = true)
	public List<DomicilioEntity> findDomicilios() {

		List<DomicilioEntity> domicilios = domicilioRepository.findAll();

		return domicilios;
	}

	@Transactional
	public DomicilioEntity update(DomicilioFrmDto domicilioFrmDto) throws ServiceException {

		PersonaEntity personaEntity = personaRepository.findOne(domicilioFrmDto.getPersonaId());

		if (personaEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra una persona con id = %s.", domicilioFrmDto.getPersonaId()));
		}

		CiudadEntity ciudadEntity = ciudadRepository.findOne(domicilioFrmDto.getCiudadId());

		if (ciudadEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra una ciudad con id = %s.", domicilioFrmDto.getCiudadId()));
		}
		
		DomicilioEntity domicilioEntity = new DomicilioEntity();

		domicilioEntity.setId(domicilioFrmDto.getId());
		domicilioEntity.setPersona(personaEntity);
		domicilioEntity.setCalle(domicilioFrmDto.getCalle());
		domicilioEntity.setNro(domicilioFrmDto.getNro());
		domicilioEntity.setPiso(domicilioFrmDto.getPiso());
		domicilioEntity.setDpto(domicilioFrmDto.getDpto());
		domicilioEntity.setCiudad(ciudadEntity);
		domicilioEntity.setDescripcion(domicilioFrmDto.getDescripcion());
		domicilioEntity.setPrincipal(domicilioFrmDto.getPrincipal());
		domicilioRepository.save(domicilioEntity);

		return domicilioEntity;

	}

	@Transactional
	public void delete(DomicilioEntity domicilioEntity) {
		domicilioRepository.delete(domicilioEntity);
	}
}
