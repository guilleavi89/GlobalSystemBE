package com.guilleavi.globalsystem.services;

import java.util.Arrays;

import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.guilleavi.globalsystem.dtos.UserDetailsImplDto;

@Service
public class AppUserDetailsService implements UserDetailsService {

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

		if (username.equals("root")) {
			return new UserDetailsImplDto("root", "root",
					Arrays.asList(new SimpleGrantedAuthority("ADMINISTRADOR"), new SimpleGrantedAuthority("USUARIO")));
		}

		throw new UsernameNotFoundException("El usuario no existe!");
	}
}
