package com.guilleavi.globalsystem.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.globalsystem.dtos.DocumentoTipoFrmDto;
import com.guilleavi.globalsystem.entities.DocumentoTipoEntity;
import com.guilleavi.globalsystem.exceptions.ServiceException;
import com.guilleavi.globalsystem.repositories.DocumentoTipoRepository;

@Service
public class DocumentoTipoService {

	@Autowired
	private DocumentoTipoRepository documentoTipoRepository = null;

	@Transactional(readOnly = true)
	public Boolean exists(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					"El 'id' de tipo de documento no puede ser nulo.");
		}

		DocumentoTipoEntity documentoTipoEntity = documentoTipoRepository.findOne(id);

		if (documentoTipoEntity == null) {
			return false;
		} else {
			return true;
		}

	}

	@Transactional(readOnly = true)
	public DocumentoTipoEntity findById(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA,
					"El 'id' de tipo de documento no puede ser nulo.");
		}

		DocumentoTipoEntity documentoTipoEntity = documentoTipoRepository.findOne(id);

		if (documentoTipoEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra un tipo de documento con id = %s.", id));
		}

		return documentoTipoEntity;
	}

	@Transactional(readOnly = true)
	public List<DocumentoTipoEntity> findDocumentoTipos() {

		List<DocumentoTipoEntity> documentoTipos = documentoTipoRepository.findAll();

		return documentoTipos;
	}

	@Transactional
	public DocumentoTipoEntity update(DocumentoTipoFrmDto documentoTipoFrmDto) throws ServiceException {

		DocumentoTipoEntity documentoTipoEntity = new DocumentoTipoEntity();

		documentoTipoEntity.setId(documentoTipoFrmDto.getId());
		documentoTipoEntity.setDescripcion(documentoTipoFrmDto.getDescripcion());
		documentoTipoEntity.setDescripcionReducida(documentoTipoFrmDto.getDescripcionReducida());
		documentoTipoRepository.save(documentoTipoEntity);

		return documentoTipoEntity;

	}

	@Transactional
	public void delete(DocumentoTipoEntity documentoTipoEntity) {
		documentoTipoRepository.delete(documentoTipoEntity);
	}
}
