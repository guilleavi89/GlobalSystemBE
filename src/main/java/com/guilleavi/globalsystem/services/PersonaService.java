package com.guilleavi.globalsystem.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.globalsystem.dtos.PersonaFrmDto;
import com.guilleavi.globalsystem.entities.DocumentoTipoEntity;
import com.guilleavi.globalsystem.entities.PersonaEntity;
import com.guilleavi.globalsystem.entities.PersonaTipoEntity;
import com.guilleavi.globalsystem.exceptions.ServiceException;
import com.guilleavi.globalsystem.repositories.DocumentoTipoRepository;
import com.guilleavi.globalsystem.repositories.PersonaRepository;
import com.guilleavi.globalsystem.repositories.PersonaTipoRepository;

@Service
public class PersonaService {

	@Autowired
	private PersonaRepository personaRepository = null;
	@Autowired
	private PersonaTipoRepository personaTipoRepository = null;
	@Autowired
	private DocumentoTipoRepository documentoTipoRepository = null;

	@Transactional(readOnly = true)
	public Boolean exists(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		PersonaEntity personaEntity = personaRepository.findOne(id);

		if (personaEntity == null) {
			return false;
		} else {
			return true;
		}

	}

	@Transactional(readOnly = true)
	public PersonaEntity findById(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		PersonaEntity personaEntity = personaRepository.findOne(id);

		if (personaEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra una persona con id = %s.", id));
		}

		return personaEntity;
	}

	@Transactional(readOnly = true)
	public List<PersonaEntity> findPersonas() {

		List<PersonaEntity> personas = personaRepository.findAll();

		return personas;
	}

	@Transactional
	public PersonaEntity update(PersonaFrmDto personaFrmDto) throws ServiceException {

		PersonaTipoEntity personaTipoEntity = personaTipoRepository.findOne(personaFrmDto.getTipoId());

		if (personaTipoEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra un tipo de persona con id = %s.", personaFrmDto.getTipoId()));
		}

		DocumentoTipoEntity documentoTipoEntity = documentoTipoRepository.findOne(personaFrmDto.getDocumentoTipoId());

		if (documentoTipoEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND, String
					.format("No se encuentra un tipo de documento con id = %s.", personaFrmDto.getDocumentoTipoId()));
		}

		PersonaEntity personaEntity = new PersonaEntity();

		personaEntity.setId(personaFrmDto.getId());
		personaEntity.setNombre(personaFrmDto.getNombre());
		personaEntity.setTipo(personaTipoEntity);
		personaEntity.setDocumentoTipo(documentoTipoEntity);
		personaEntity.setDocumentoNumero(personaFrmDto.getDocumentoNumero());
		personaRepository.save(personaEntity);

		return personaEntity;

	}

	@Transactional
	public void delete(PersonaEntity personaEntity) {
		personaRepository.delete(personaEntity);
	}
}
