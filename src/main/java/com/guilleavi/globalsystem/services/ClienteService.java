package com.guilleavi.globalsystem.services;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.google.common.base.CharMatcher;
import com.guilleavi.globalsystem.dtos.ClienteFrmDto;
import com.guilleavi.globalsystem.entities.ClienteEntity;
import com.guilleavi.globalsystem.entities.EstadoEntity;
import com.guilleavi.globalsystem.entities.IvaTipoEntity;
import com.guilleavi.globalsystem.entities.PersonaEntity;
import com.guilleavi.globalsystem.entities.QClienteEntity;
import com.guilleavi.globalsystem.exceptions.ServiceException;
import com.guilleavi.globalsystem.repositories.ClienteRepository;
import com.guilleavi.globalsystem.repositories.EstadoRepository;
import com.guilleavi.globalsystem.repositories.IvaTipoRepository;
import com.guilleavi.globalsystem.repositories.PersonaRepository;
import com.guilleavi.globalsystem.utils.NumbersUtil;
import com.mysema.query.jpa.impl.JPAQuery;

@Service
public class ClienteService {

	@PersistenceContext
	protected EntityManager em = null;

	@Autowired
	private ClienteRepository clienteRepository = null;
	@Autowired
	private PersonaRepository personaRepository = null;
	@Autowired
	private IvaTipoRepository ivaTipoRepository = null;
	@Autowired
	private EstadoRepository estadoRepository = null;

	@Transactional(readOnly = true)
	public Boolean exists(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de cliente no puede ser nulo.");
		}

		ClienteEntity clienteEntity = clienteRepository.findOne(id);

		if (clienteEntity == null) {
			return false;
		} else {
			return true;
		}

	}

	@Transactional(readOnly = true)
	public ClienteEntity findById(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de cliente no puede ser nulo.");
		}

		ClienteEntity clienteEntity = clienteRepository.findOne(id);

		if (clienteEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra un cliente con id = %s.", id));
		}

		return clienteEntity;
	}

	@Transactional(readOnly = true)
	public List<ClienteEntity> findClientes() {

		List<ClienteEntity> clientes = clienteRepository.findAll();

		return clientes;
	}

	@Transactional(readOnly = true)
	public List<ClienteEntity> findByKeyword(String keyword) {

		JPAQuery query = new JPAQuery(em);

		QClienteEntity qCliente = QClienteEntity.clienteEntity;

		List<ClienteEntity> clientes = null;

		if (NumbersUtil.isNumeric(CharMatcher.is('\'').trimFrom(keyword))) {

			clientes = query
					.from(qCliente).where(
							qCliente.persona.nombre.contains(CharMatcher.is('\'').trimFrom(keyword))
									.or(qCliente.persona.documentoNumero
											.eq(Long.parseLong(CharMatcher.is('\'').trimFrom(keyword)))))
					.list(qCliente);
		} else {

			clientes = query.from(qCliente)
					.where(qCliente.persona.nombre.contains(CharMatcher.is('\'').trimFrom(keyword))).list(qCliente);
		}

		return clientes;
	}

	@Transactional
	public ClienteEntity update(ClienteFrmDto clienteFrmDto) throws ServiceException {

		PersonaEntity personaEntity = personaRepository.findOne(clienteFrmDto.getPersonaId());

		if (personaEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra una persona con id = %s.", clienteFrmDto.getPersonaId()));
		}

		IvaTipoEntity ivaTipoEntity = ivaTipoRepository.findOne(clienteFrmDto.getIvaTipoId());

		if (ivaTipoEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra un tipo de iva con id = %s.", clienteFrmDto.getIvaTipoId()));
		}

		EstadoEntity estadoEntity = estadoRepository.findOne(clienteFrmDto.getEstadoId());

		if (estadoEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra un estado con id = %s.", clienteFrmDto.getEstadoId()));
		}

		ClienteEntity clienteEntity = new ClienteEntity();

		clienteEntity.setId(clienteFrmDto.getId());
		clienteEntity.setPersona(personaEntity);
		clienteEntity.setIvaTipo(ivaTipoEntity);
		clienteEntity.setEstado(estadoEntity);
		clienteRepository.save(clienteEntity);

		return clienteEntity;

	}

	@Transactional
	public void delete(ClienteEntity clienteEntity) {
		clienteRepository.delete(clienteEntity);
	}
}
