package com.guilleavi.globalsystem.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.globalsystem.dtos.PersonaTipoFrmDto;
import com.guilleavi.globalsystem.entities.PersonaTipoEntity;
import com.guilleavi.globalsystem.exceptions.ServiceException;
import com.guilleavi.globalsystem.repositories.PersonaTipoRepository;

@Service
public class PersonaTipoService {

	@Autowired
	private PersonaTipoRepository personaTipoRepository = null;

	@Transactional(readOnly = true)
	public Boolean exists(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de tipo de persona no puede ser nulo.");
		}

		PersonaTipoEntity personaTipoEntity = personaTipoRepository.findOne(id);

		if (personaTipoEntity == null) {
			return false;
		} else {
			return true;
		}

	}

	@Transactional(readOnly = true)
	public PersonaTipoEntity findById(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de tipo de persona no puede ser nulo.");
		}

		PersonaTipoEntity personaTipoEntity = personaTipoRepository.findOne(id);

		if (personaTipoEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra un tipo de persona con id = %s.", id));
		}

		return personaTipoEntity;
	}

	@Transactional(readOnly = true)
	public List<PersonaTipoEntity> findPersonaTipos() {

		List<PersonaTipoEntity> personaTipos = personaTipoRepository.findAll();

		return personaTipos;
	}

	@Transactional
	public PersonaTipoEntity update(PersonaTipoFrmDto personaTipoFrmDto) throws ServiceException {

		PersonaTipoEntity personaTipoEntity = new PersonaTipoEntity();

		personaTipoEntity.setId(personaTipoFrmDto.getId());
		personaTipoEntity.setDescripcion(personaTipoFrmDto.getDescripcion());
		personaTipoRepository.save(personaTipoEntity);

		return personaTipoEntity;

	}

	@Transactional
	public void delete(PersonaTipoEntity personaTipoEntity) {
		personaTipoRepository.delete(personaTipoEntity);
	}
}
