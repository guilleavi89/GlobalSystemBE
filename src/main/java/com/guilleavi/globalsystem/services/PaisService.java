package com.guilleavi.globalsystem.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.globalsystem.dtos.PaisFrmDto;
import com.guilleavi.globalsystem.entities.PaisEntity;
import com.guilleavi.globalsystem.exceptions.ServiceException;
import com.guilleavi.globalsystem.repositories.PaisRepository;

@Service
public class PaisService {

	@Autowired
	private PaisRepository paisRepository = null;

	@Transactional(readOnly = true)
	public Boolean exists(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de pais no puede ser nulo.");
		}

		PaisEntity paisEntity = paisRepository.findOne(id);

		if (paisEntity == null) {
			return false;
		} else {
			return true;
		}

	}

	@Transactional(readOnly = true)
	public PaisEntity findById(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de pais no puede ser nulo.");
		}

		PaisEntity paisEntity = paisRepository.findOne(id);

		if (paisEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra un pais con id = %s.", id));
		}

		return paisEntity;
	}

	@Transactional(readOnly = true)
	public List<PaisEntity> findPaiss() {

		List<PaisEntity> paiss = paisRepository.findAll();

		return paiss;
	}

	@Transactional
	public PaisEntity update(PaisFrmDto paisFrmDto) throws ServiceException {

		PaisEntity paisEntity = new PaisEntity();

		paisEntity.setId(paisFrmDto.getId());
		paisEntity.setNombre(paisFrmDto.getNombre());
		paisEntity.setPrefijoTelefonico(paisFrmDto.getPrefijoTelefonico());
		paisRepository.save(paisEntity);

		return paisEntity;

	}

	@Transactional
	public void delete(PaisEntity paisEntity) {
		paisRepository.delete(paisEntity);
	}
}
