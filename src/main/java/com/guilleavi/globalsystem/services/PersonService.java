package com.guilleavi.globalsystem.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.globalsystem.dtos.PersonFrmDto;
import com.guilleavi.globalsystem.entities.PersonEntity;
import com.guilleavi.globalsystem.exceptions.ServiceException;
import com.guilleavi.globalsystem.repositories.PersonRepository;

@Service
public class PersonService {

	@Autowired
	private PersonRepository personRepository = null;
	
	@Transactional(readOnly = true)
	public Boolean exists(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		PersonEntity personEntity = personRepository.findOne(id);

		if (personEntity == null) {
			return false;
		} else {
			return true;
		}

	}
	
	@Transactional(readOnly = true)
	public PersonEntity findById(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de persona no puede ser nulo.");
		}

		PersonEntity personEntity = personRepository.findOne(id);

		if (personEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra una persona con id = %s.", id));
		}

		return personEntity;
	}

	@Transactional(readOnly = true)
	public List<PersonEntity> findPeople() {

		List<PersonEntity> people = personRepository.findAll();

		return people;
	}

	@Transactional
	public PersonEntity update(PersonFrmDto personFrmDto) {

		PersonEntity personEntity = new PersonEntity();

		personEntity.setId(personFrmDto.getId());
		personEntity.setLastname(personFrmDto.getLastname());
		personEntity.setName(personFrmDto.getName());
		personRepository.save(personEntity);

		return personEntity;

	}

	@Transactional
	public void delete(PersonEntity personEntity) {
		personRepository.delete(personEntity);
	}
}
