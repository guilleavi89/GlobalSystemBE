package com.guilleavi.globalsystem.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.globalsystem.dtos.IvaTipoFrmDto;
import com.guilleavi.globalsystem.entities.IvaTipoEntity;
import com.guilleavi.globalsystem.exceptions.ServiceException;
import com.guilleavi.globalsystem.repositories.IvaTipoRepository;

@Service
public class IvaTipoService {

	@Autowired
	private IvaTipoRepository ivaTipoRepository = null;

	@Transactional(readOnly = true)
	public Boolean exists(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de tipo de iva no puede ser nulo.");
		}

		IvaTipoEntity ivaTipoEntity = ivaTipoRepository.findOne(id);

		if (ivaTipoEntity == null) {
			return false;
		} else {
			return true;
		}

	}

	@Transactional(readOnly = true)
	public IvaTipoEntity findById(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de tipo de iva no puede ser nulo.");
		}

		IvaTipoEntity ivaTipoEntity = ivaTipoRepository.findOne(id);

		if (ivaTipoEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra un tipo de iva con id = %s.", id));
		}

		return ivaTipoEntity;
	}

	@Transactional(readOnly = true)
	public List<IvaTipoEntity> findIvaTipos() {

		List<IvaTipoEntity> ivaTipos = ivaTipoRepository.findAll();

		return ivaTipos;
	}

	@Transactional
	public IvaTipoEntity update(IvaTipoFrmDto ivaTipoFrmDto) throws ServiceException {

		IvaTipoEntity ivaTipoEntity = new IvaTipoEntity();

		ivaTipoEntity.setId(ivaTipoFrmDto.getId());
		ivaTipoEntity.setDescripcion(ivaTipoFrmDto.getDescripcion());
		ivaTipoRepository.save(ivaTipoEntity);

		return ivaTipoEntity;

	}

	@Transactional
	public void delete(IvaTipoEntity ivaTipoEntity) {
		ivaTipoRepository.delete(ivaTipoEntity);
	}
}
