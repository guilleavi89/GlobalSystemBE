package com.guilleavi.globalsystem.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.guilleavi.globalsystem.dtos.CiudadFrmDto;
import com.guilleavi.globalsystem.entities.CiudadEntity;
import com.guilleavi.globalsystem.entities.ProvinciaEntity;
import com.guilleavi.globalsystem.exceptions.ServiceException;
import com.guilleavi.globalsystem.repositories.CiudadRepository;
import com.guilleavi.globalsystem.repositories.ProvinciaRepository;

@Service
public class CiudadService {

	@Autowired
	private CiudadRepository ciudadRepository = null;
	@Autowired
	private ProvinciaRepository provinciaRepository = null;

	@Transactional(readOnly = true)
	public Boolean exists(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de ciudad no puede ser nulo.");
		}

		CiudadEntity ciudadEntity = ciudadRepository.findOne(id);

		if (ciudadEntity == null) {
			return false;
		} else {
			return true;
		}

	}

	@Transactional(readOnly = true)
	public CiudadEntity findById(Integer id) throws ServiceException {

		if (id == null) {
			throw new ServiceException(ServiceException.MISSING_DATA, "El 'id' de ciudad no puede ser nulo.");
		}

		CiudadEntity ciudadEntity = ciudadRepository.findOne(id);

		if (ciudadEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra una ciudad con id = %s.", id));
		}

		return ciudadEntity;
	}

	@Transactional(readOnly = true)
	public List<CiudadEntity> findCiudades() {

		List<CiudadEntity> ciudades = ciudadRepository.findAll();

		return ciudades;
	}

	@Transactional
	public CiudadEntity update(CiudadFrmDto ciudadFrmDto) throws ServiceException {
		
		ProvinciaEntity provinciaEntity = provinciaRepository.findOne(ciudadFrmDto.getProvinciaId());

		if (provinciaEntity == null) {
			throw new ServiceException(ServiceException.ENTITY_NOT_FOUND,
					String.format("No se encuentra una provincia con id = %s.", ciudadFrmDto.getProvinciaId()));
		}

		CiudadEntity ciudadEntity = new CiudadEntity();

		ciudadEntity.setId(ciudadFrmDto.getId());
		ciudadEntity.setNombre(ciudadFrmDto.getNombre());
		ciudadEntity.setCodigoPostal(ciudadFrmDto.getCodigoPostal());
		ciudadEntity.setProvincia(provinciaEntity);
		ciudadEntity.setPrefijoTelefonico(ciudadFrmDto.getPrefijoTelefonico());
		ciudadRepository.save(ciudadEntity);

		return ciudadEntity;

	}

	@Transactional
	public void delete(CiudadEntity ciudadEntity) {
		ciudadRepository.delete(ciudadEntity);
	}
}
