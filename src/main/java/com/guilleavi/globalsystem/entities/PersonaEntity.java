package com.guilleavi.globalsystem.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table (name = "persona")
public class PersonaEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;
	
	@Column(name = "nombre", nullable = false, length = 200)
	private String nombre = null;	
	
	@OneToOne
	@JoinColumn(name = "tipo_id", referencedColumnName = "id", nullable = false)
	private PersonaTipoEntity tipo = null;	
	
	@OneToOne
	@JoinColumn(name = "documento_tipo_id", referencedColumnName = "id", nullable = true)
	private DocumentoTipoEntity documentoTipo = null;	

	@Column(name = "documento_numero", nullable = true, scale = 18)
	private Long documentoNumero = null;	

	public PersonaEntity() {
		super();
	}

	public PersonaEntity(Integer id, String nombre, PersonaTipoEntity tipo, DocumentoTipoEntity documentoTipo,
			Long documentoNumero) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
		this.documentoTipo = documentoTipo;
		this.documentoNumero = documentoNumero;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public PersonaTipoEntity getTipo() {
		return tipo;
	}

	public void setTipo(PersonaTipoEntity tipo) {
		this.tipo = tipo;
	}

	public DocumentoTipoEntity getDocumentoTipo() {
		return documentoTipo;
	}

	public void setDocumentoTipo(DocumentoTipoEntity documentoTipo) {
		this.documentoTipo = documentoTipo;
	}

	public Long getDocumentoNumero() {
		return documentoNumero;
	}

	public void setDocumentoNumero(Long documentoNumero) {
		this.documentoNumero = documentoNumero;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PersonaEntity other = (PersonaEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PersonaEntity [id=" + id + ", nombre=" + nombre + "]";
	}	

}
