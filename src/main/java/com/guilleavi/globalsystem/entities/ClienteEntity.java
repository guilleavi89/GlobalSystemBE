package com.guilleavi.globalsystem.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "cliente")
public class ClienteEntity implements Serializable {

	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", unique = true, nullable = false)
	private Integer id = null;

	@OneToOne
	@JoinColumn(name = "persona_id", referencedColumnName = "id", nullable = false)
	private PersonaEntity persona = null;

	@OneToOne
	@JoinColumn(name = "iva_tipo_id", referencedColumnName = "id", nullable = false)
	private IvaTipoEntity ivaTipo = null;

	@OneToOne
	@JoinColumn(name = "estado_id", referencedColumnName = "id", nullable = false)
	private EstadoEntity estado = null;

	public ClienteEntity() {
		super();
	}

	public ClienteEntity(Integer id, PersonaEntity persona, IvaTipoEntity ivaTipo, EstadoEntity estado) {
		super();
		this.id = id;
		this.persona = persona;
		this.ivaTipo = ivaTipo;
		this.estado = estado;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PersonaEntity getPersona() {
		return persona;
	}

	public void setPersona(PersonaEntity persona) {
		this.persona = persona;
	}

	public IvaTipoEntity getIvaTipo() {
		return ivaTipo;
	}

	public void setIvaTipo(IvaTipoEntity ivaTipo) {
		this.ivaTipo = ivaTipo;
	}

	public EstadoEntity getEstado() {
		return estado;
	}

	public void setEstado(EstadoEntity estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ClienteEntity other = (ClienteEntity) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ClienteEntity [id=" + id + "]";
	}

}
