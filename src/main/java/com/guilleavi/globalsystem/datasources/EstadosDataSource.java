package com.guilleavi.globalsystem.datasources;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.OrderByOption;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cairone.olingo.ext.jpa.query.JPQLQuery;
import com.cairone.olingo.ext.jpa.query.JPQLQueryBuilder;
import com.guilleavi.globalsystem.dtos.EstadoFrmDto;
import com.guilleavi.globalsystem.dtos.validators.EstadoFrmDtoValidator;
import com.guilleavi.globalsystem.edm.resources.EstadoEdm;
import com.guilleavi.globalsystem.entities.EstadoEntity;
import com.guilleavi.globalsystem.exceptions.ODataBadRequestException;
import com.guilleavi.globalsystem.services.EstadoService;
import com.guilleavi.globalsystem.utils.OdataExceptionParser;
import com.guilleavi.globalsystem.utils.ValidatorUtil;

@Component
public class EstadosDataSource extends AbstractDataSource {

	@Autowired
	private EstadoService estadoService = null;
	@Autowired
	private EstadoFrmDtoValidator estadoFrmDtoValidator = null;

	@Override
	public String isSuitableFor() {
		return EstadoEdm.ENTITY_SET;
	}

	@Override
	public Object create(Object entity) throws ODataApplicationException {

		if (entity instanceof EstadoEdm) {

			EstadoEdm estadoEdm = (EstadoEdm) entity;

			try {
				EstadoFrmDto estadoFrmDto = new EstadoFrmDto(estadoEdm);

				ValidatorUtil.validate(estadoFrmDtoValidator, messageSource, estadoFrmDto);
				EstadoEntity estadoEntity = estadoService.update(estadoFrmDto);

				return new EstadoEdm(estadoEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad estado.");

	}

	@Override
	public Object update(Map<String, UriParameter> keyPredicateMap, Object entity, List<String> propertiesInJSON,
			boolean isPut) throws ODataApplicationException {

		if (entity instanceof EstadoEdm) {

			Integer id = Integer.valueOf(keyPredicateMap.get(EstadoEdm.PROPERTY_ID).getText());

			EstadoEdm estadoEdm = (EstadoEdm) entity;

			try {

				EstadoFrmDto estadoFrmDto = new EstadoFrmDto(estadoEdm);

				estadoFrmDto.setId(id);

				EstadoEntity estadoEntity = estadoService.findById(id);

				if (!isPut) {
					if (estadoFrmDto.getDescripcion() == null
							&& !propertiesInJSON.contains(EstadoEdm.PROPERTY_DESCRIPCION)) {
						estadoFrmDto.setDescripcion(estadoEntity.getDescripcion());
					}
				}

				ValidatorUtil.validate(estadoFrmDtoValidator, messageSource, estadoFrmDto);
				estadoEntity = estadoService.update(estadoFrmDto);

				return new EstadoEdm(estadoEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad estado.");

	}

	@Override
	public Object delete(Map<String, UriParameter> keyPredicateMap) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(EstadoEdm.PROPERTY_ID).getText());

		try {
			EstadoEntity estadoEntity = estadoService.findById(id);
			estadoService.delete(estadoEntity);

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}

		return null;
	}

	@Override
	public Object readFromKey(Map<String, UriParameter> keyPredicateMap, ExpandOption expandOption,
			SelectOption selectOption) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(EstadoEdm.PROPERTY_ID).getText());

		try {
			EstadoEntity estadoEntity = estadoService.findById(id);
			EstadoEdm estadoEdm = new EstadoEdm(estadoEntity);

			return estadoEdm;

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}
	}

	@Override
	public Iterable<?> readAll(ExpandOption expandOption, FilterOption filterOption, OrderByOption orderByOption)
			throws ODataApplicationException {

		JPQLQuery query = new JPQLQueryBuilder().setDistinct(false).setClazz(EstadoEdm.class)
				.setExpandOption(expandOption).setFilterOption(filterOption).setOrderByOption(orderByOption).build();

		List<EstadoEntity> estadoEntities = JPQLQuery.execute(entityManager, query);
		List<EstadoEdm> estadoEdms = estadoEntities.stream().map(entity -> {
			EstadoEdm estadoEdm = new EstadoEdm(entity);
			return estadoEdm;
		}).collect(Collectors.toList());

		return estadoEdms;
	}

}
