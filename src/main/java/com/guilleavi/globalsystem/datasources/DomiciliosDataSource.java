package com.guilleavi.globalsystem.datasources;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.OrderByOption;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cairone.olingo.ext.jpa.query.JPQLQuery;
import com.cairone.olingo.ext.jpa.query.JPQLQueryBuilder;
import com.guilleavi.globalsystem.dtos.DomicilioFrmDto;
import com.guilleavi.globalsystem.dtos.validators.DomicilioFrmDtoValidator;
import com.guilleavi.globalsystem.edm.resources.DomicilioEdm;
import com.guilleavi.globalsystem.entities.DomicilioEntity;
import com.guilleavi.globalsystem.exceptions.ODataBadRequestException;
import com.guilleavi.globalsystem.services.DomicilioService;
import com.guilleavi.globalsystem.utils.OdataExceptionParser;
import com.guilleavi.globalsystem.utils.ValidatorUtil;

@Component
public class DomiciliosDataSource extends AbstractDataSource {

	@Autowired
	private DomicilioService domicilioService = null;
	@Autowired
	private DomicilioFrmDtoValidator domicilioFrmDtoValidator = null;

	@Override
	public String isSuitableFor() {
		return DomicilioEdm.ENTITY_SET;
	}

	@Override
	public Object create(Object entity) throws ODataApplicationException {

		if (entity instanceof DomicilioEdm) {

			DomicilioEdm domicilioEdm = (DomicilioEdm) entity;

			try {
				DomicilioFrmDto domicilioFrmDto = new DomicilioFrmDto(domicilioEdm);

				ValidatorUtil.validate(domicilioFrmDtoValidator, messageSource, domicilioFrmDto);
				DomicilioEntity domicilioEntity = domicilioService.update(domicilioFrmDto);

				return new DomicilioEdm(domicilioEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad domicilio.");

	}

	@Override
	public Object update(Map<String, UriParameter> keyPredicateMap, Object entity, List<String> propertiesInJSON,
			boolean isPut) throws ODataApplicationException {

		if (entity instanceof DomicilioEdm) {

			Integer id = Integer.valueOf(keyPredicateMap.get(DomicilioEdm.PROPERTY_ID).getText());

			DomicilioEdm domicilioEdm = (DomicilioEdm) entity;

			try {

				DomicilioFrmDto domicilioFrmDto = new DomicilioFrmDto(domicilioEdm);

				domicilioFrmDto.setId(id);

				DomicilioEntity domicilioEntity = domicilioService.findById(id);

				if (!isPut) {
					if (domicilioFrmDto.getPersonaId() == null
							&& !propertiesInJSON.contains(DomicilioEdm.PROPERTY_PERSONA)) {
						domicilioFrmDto.setPersonaId(domicilioEntity.getPersona().getId());
					}
					if (domicilioFrmDto.getCalle() == null && !propertiesInJSON.contains(DomicilioEdm.PROPERTY_CALLE)) {
						domicilioFrmDto.setCalle(domicilioEntity.getCalle());
					}
					if (domicilioFrmDto.getNro() == null && !propertiesInJSON.contains(DomicilioEdm.PROPERTY_NRO)) {
						domicilioFrmDto.setNro(domicilioEntity.getNro());
					}
					if (domicilioFrmDto.getPiso() == null && !propertiesInJSON.contains(DomicilioEdm.PROPERTY_PISO)) {
						domicilioFrmDto.setPiso(domicilioEntity.getPiso());
					}
					if (domicilioFrmDto.getDpto() == null && !propertiesInJSON.contains(DomicilioEdm.PROPERTY_DPTO)) {
						domicilioFrmDto.setDpto(domicilioEntity.getDpto());
					}
					if (domicilioFrmDto.getCiudadId() == null
							&& !propertiesInJSON.contains(DomicilioEdm.PROPERTY_CIUDAD)) {
						domicilioFrmDto.setCiudadId(domicilioEntity.getCiudad().getId());
					}
					if (domicilioFrmDto.getDescripcion() == null
							&& !propertiesInJSON.contains(DomicilioEdm.PROPERTY_DESCRIPCION)) {
						domicilioFrmDto.setDescripcion(domicilioEntity.getDescripcion());
					}
					if (domicilioFrmDto.getPrincipal() == null
							&& !propertiesInJSON.contains(DomicilioEdm.PROPERTY_PRINCIPAL)) {
						domicilioFrmDto.setPrincipal(domicilioEntity.getPrincipal());
					}
				}

				ValidatorUtil.validate(domicilioFrmDtoValidator, messageSource, domicilioFrmDto);
				domicilioEntity = domicilioService.update(domicilioFrmDto);

				return new DomicilioEdm(domicilioEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad domicilio.");

	}

	@Override
	public Object delete(Map<String, UriParameter> keyPredicateMap) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(DomicilioEdm.PROPERTY_ID).getText());

		try {
			DomicilioEntity domicilioEntity = domicilioService.findById(id);
			domicilioService.delete(domicilioEntity);

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}

		return null;
	}

	@Override
	public Object readFromKey(Map<String, UriParameter> keyPredicateMap, ExpandOption expandOption,
			SelectOption selectOption) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(DomicilioEdm.PROPERTY_ID).getText());

		try {
			DomicilioEntity domicilioEntity = domicilioService.findById(id);
			DomicilioEdm domicilioEdm = new DomicilioEdm(domicilioEntity);

			return domicilioEdm;

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}
	}

	@Override
	public Iterable<?> readAll(ExpandOption expandOption, FilterOption filterOption, OrderByOption orderByOption)
			throws ODataApplicationException {

		JPQLQuery query = new JPQLQueryBuilder().setDistinct(false).setClazz(DomicilioEdm.class)
				.setExpandOption(expandOption).setFilterOption(filterOption).setOrderByOption(orderByOption).build();

		List<DomicilioEntity> domicilioEntities = JPQLQuery.execute(entityManager, query);
		List<DomicilioEdm> domicilioEdms = domicilioEntities.stream().map(entity -> {
			DomicilioEdm domicilioEdm = new DomicilioEdm(entity);
			return domicilioEdm;
		}).collect(Collectors.toList());

		return domicilioEdms;
	}

}
