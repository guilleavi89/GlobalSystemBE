package com.guilleavi.globalsystem.datasources;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.OrderByOption;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cairone.olingo.ext.jpa.query.JPQLQuery;
import com.cairone.olingo.ext.jpa.query.JPQLQueryBuilder;
import com.guilleavi.globalsystem.dtos.PersonFrmDto;
import com.guilleavi.globalsystem.dtos.validators.PersonFrmDtoValidator;
import com.guilleavi.globalsystem.edm.resources.PersonEdm;
import com.guilleavi.globalsystem.entities.PersonEntity;
import com.guilleavi.globalsystem.exceptions.ODataBadRequestException;
import com.guilleavi.globalsystem.services.PersonService;
import com.guilleavi.globalsystem.utils.OdataExceptionParser;
import com.guilleavi.globalsystem.utils.ValidatorUtil;

@Component
public class PeopleDataSource extends AbstractDataSource {

	@Autowired
	private PersonService personService = null;
	@Autowired
	private PersonFrmDtoValidator personFrmDtoValidator = null;

	@Override
	public String isSuitableFor() {
		return PersonEdm.ENTITY_SET;
	}

	@Override
	public Object create(Object entity) throws ODataApplicationException {

		if (entity instanceof PersonEdm) {

			PersonEdm personEdm = (PersonEdm) entity;

			PersonFrmDto personFrmDto = new PersonFrmDto(personEdm);

			ValidatorUtil.validate(personFrmDtoValidator, messageSource, personFrmDto);
			PersonEntity personEntity = personService.update(personFrmDto);

			return new PersonEdm(personEntity);

		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad persona.");
	}

	@Override
	public Object update(Map<String, UriParameter> keyPredicateMap, Object entity, List<String> propertiesInJSON,
			boolean isPut) throws ODataApplicationException {

		if (entity instanceof PersonEdm) {

			Integer id = Integer.valueOf(keyPredicateMap.get("Id").getText());

			PersonEdm personEdm = (PersonEdm) entity;

			try {

				PersonFrmDto personFrmDto = new PersonFrmDto(personEdm);

				personFrmDto.setId(id);

				PersonEntity personEntity = personService.findById(id);

				if (!isPut) {
					if (personFrmDto.getLastname() == null && !propertiesInJSON.contains("Lastname")) {
						personFrmDto.setLastname(personEntity.getLastname());
					}
					if (personFrmDto.getName() == null && !propertiesInJSON.contains("Name")) {
						personFrmDto.setName(personEntity.getName());
					}
				}

				ValidatorUtil.validate(personFrmDtoValidator, messageSource, personFrmDto);
				personEntity = personService.update(personFrmDto);

				return new PersonEdm(personEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad persona.");

	}

	@Override
	public Object delete(Map<String, UriParameter> keyPredicateMap) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get("Id").getText());

		try {
			PersonEntity personEntity = personService.findById(id);
			personService.delete(personEntity);

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}

		return null;
	}

	@Override
	public Object readFromKey(Map<String, UriParameter> keyPredicateMap, ExpandOption expandOption,
			SelectOption selectOption) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get("Id").getText());

		try {
			PersonEntity personEntity = personService.findById(id);
			PersonEdm personEdm = new PersonEdm(personEntity);

			return personEdm;

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}
	}

	@Override
	public Iterable<?> readAll(ExpandOption expandOption, FilterOption filterOption, OrderByOption orderByOption)
			throws ODataApplicationException {

		JPQLQuery query = new JPQLQueryBuilder().setDistinct(false).setClazz(PersonEdm.class)
				.setExpandOption(expandOption).setFilterOption(filterOption).setOrderByOption(orderByOption).build();

		List<PersonEntity> personEntities = JPQLQuery.execute(entityManager, query);
		List<PersonEdm> personEdms = personEntities.stream().map(entity -> {
			PersonEdm personEdm = new PersonEdm(entity);
			return personEdm;
		}).collect(Collectors.toList());

		return personEdms;
	}
}
