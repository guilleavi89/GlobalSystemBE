package com.guilleavi.globalsystem.datasources;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.OrderByOption;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cairone.olingo.ext.jpa.query.JPQLQuery;
import com.cairone.olingo.ext.jpa.query.JPQLQueryBuilder;
import com.guilleavi.globalsystem.dtos.IvaTipoFrmDto;
import com.guilleavi.globalsystem.dtos.validators.IvaTipoFrmDtoValidator;
import com.guilleavi.globalsystem.edm.resources.IvaTipoEdm;
import com.guilleavi.globalsystem.entities.IvaTipoEntity;
import com.guilleavi.globalsystem.exceptions.ODataBadRequestException;
import com.guilleavi.globalsystem.services.IvaTipoService;
import com.guilleavi.globalsystem.utils.OdataExceptionParser;
import com.guilleavi.globalsystem.utils.ValidatorUtil;

@Component
public class IvaTiposDataSource extends AbstractDataSource {

	@Autowired
	private IvaTipoService ivaTipoService = null;
	@Autowired
	private IvaTipoFrmDtoValidator ivaTipoFrmDtoValidator = null;

	@Override
	public String isSuitableFor() {
		return IvaTipoEdm.ENTITY_SET;
	}

	@Override
	public Object create(Object entity) throws ODataApplicationException {

		if (entity instanceof IvaTipoEdm) {

			IvaTipoEdm ivaTipoEdm = (IvaTipoEdm) entity;

			try {
				IvaTipoFrmDto ivaTipoFrmDto = new IvaTipoFrmDto(ivaTipoEdm);

				ValidatorUtil.validate(ivaTipoFrmDtoValidator, messageSource, ivaTipoFrmDto);
				IvaTipoEntity ivaTipoEntity = ivaTipoService.update(ivaTipoFrmDto);

				return new IvaTipoEdm(ivaTipoEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad  tipo de iva.");

	}

	@Override
	public Object update(Map<String, UriParameter> keyPredicateMap, Object entity, List<String> propertiesInJSON,
			boolean isPut) throws ODataApplicationException {

		if (entity instanceof IvaTipoEdm) {

			Integer id = Integer.valueOf(keyPredicateMap.get(IvaTipoEdm.PROPERTY_ID).getText());

			IvaTipoEdm ivaTipoEdm = (IvaTipoEdm) entity;

			try {

				IvaTipoFrmDto ivaTipoFrmDto = new IvaTipoFrmDto(ivaTipoEdm);

				ivaTipoFrmDto.setId(id);

				IvaTipoEntity ivaTipoEntity = ivaTipoService.findById(id);

				if (!isPut) {
					if (ivaTipoFrmDto.getDescripcion() == null
							&& !propertiesInJSON.contains(IvaTipoEdm.PROPERTY_DESCRIPCION)) {
						ivaTipoFrmDto.setDescripcion(ivaTipoEntity.getDescripcion());
					}
				}

				ValidatorUtil.validate(ivaTipoFrmDtoValidator, messageSource, ivaTipoFrmDto);
				ivaTipoEntity = ivaTipoService.update(ivaTipoFrmDto);

				return new IvaTipoEdm(ivaTipoEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad tipo de iva.");

	}

	@Override
	public Object delete(Map<String, UriParameter> keyPredicateMap) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(IvaTipoEdm.PROPERTY_ID).getText());

		try {
			IvaTipoEntity ivaTipoEntity = ivaTipoService.findById(id);
			ivaTipoService.delete(ivaTipoEntity);

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}

		return null;
	}

	@Override
	public Object readFromKey(Map<String, UriParameter> keyPredicateMap, ExpandOption expandOption,
			SelectOption selectOption) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(IvaTipoEdm.PROPERTY_ID).getText());

		try {
			IvaTipoEntity ivaTipoEntity = ivaTipoService.findById(id);
			IvaTipoEdm ivaTipoEdm = new IvaTipoEdm(ivaTipoEntity);

			return ivaTipoEdm;

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}
	}

	@Override
	public Iterable<?> readAll(ExpandOption expandOption, FilterOption filterOption, OrderByOption orderByOption)
			throws ODataApplicationException {

		JPQLQuery query = new JPQLQueryBuilder().setDistinct(false).setClazz(IvaTipoEdm.class)
				.setExpandOption(expandOption).setFilterOption(filterOption).setOrderByOption(orderByOption).build();

		List<IvaTipoEntity> ivaTipoEntities = JPQLQuery.execute(entityManager, query);
		List<IvaTipoEdm> ivaTipoEdms = ivaTipoEntities.stream().map(entity -> {
			IvaTipoEdm ivaTipoEdm = new IvaTipoEdm(entity);
			return ivaTipoEdm;
		}).collect(Collectors.toList());

		return ivaTipoEdms;
	}

}
