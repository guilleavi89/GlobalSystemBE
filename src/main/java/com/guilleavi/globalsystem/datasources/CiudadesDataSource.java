package com.guilleavi.globalsystem.datasources;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.OrderByOption;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cairone.olingo.ext.jpa.query.JPQLQuery;
import com.cairone.olingo.ext.jpa.query.JPQLQueryBuilder;
import com.guilleavi.globalsystem.dtos.CiudadFrmDto;
import com.guilleavi.globalsystem.dtos.validators.CiudadFrmDtoValidator;
import com.guilleavi.globalsystem.edm.resources.CiudadEdm;
import com.guilleavi.globalsystem.entities.CiudadEntity;
import com.guilleavi.globalsystem.exceptions.ODataBadRequestException;
import com.guilleavi.globalsystem.services.CiudadService;
import com.guilleavi.globalsystem.utils.OdataExceptionParser;
import com.guilleavi.globalsystem.utils.ValidatorUtil;

@Component
public class CiudadesDataSource extends AbstractDataSource {

	@Autowired
	private CiudadService ciudadService = null;
	@Autowired
	private CiudadFrmDtoValidator ciudadFrmDtoValidator = null;

	@Override
	public String isSuitableFor() {
		return CiudadEdm.ENTITY_SET;
	}

	@Override
	public Object create(Object entity) throws ODataApplicationException {

		if (entity instanceof CiudadEdm) {

			CiudadEdm ciudadEdm = (CiudadEdm) entity;

			try {
				CiudadFrmDto ciudadFrmDto = new CiudadFrmDto(ciudadEdm);

				ValidatorUtil.validate(ciudadFrmDtoValidator, messageSource, ciudadFrmDto);
				CiudadEntity ciudadEntity = ciudadService.update(ciudadFrmDto);

				return new CiudadEdm(ciudadEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad ciudad.");

	}

	@Override
	public Object update(Map<String, UriParameter> keyPredicateMap, Object entity, List<String> propertiesInJSON,
			boolean isPut) throws ODataApplicationException {

		if (entity instanceof CiudadEdm) {

			Integer id = Integer.valueOf(keyPredicateMap.get(CiudadEdm.PROPERTY_ID).getText());

			CiudadEdm ciudadEdm = (CiudadEdm) entity;

			try {

				CiudadFrmDto ciudadFrmDto = new CiudadFrmDto(ciudadEdm);

				ciudadFrmDto.setId(id);

				CiudadEntity ciudadEntity = ciudadService.findById(id);

				if (!isPut) {
					if (ciudadFrmDto.getNombre() == null && !propertiesInJSON.contains(CiudadEdm.PROPERTY_NOMBRE)) {
						ciudadFrmDto.setNombre(ciudadEntity.getNombre());
					}
					if (ciudadFrmDto.getCodigoPostal() == null
							&& !propertiesInJSON.contains(CiudadEdm.PROPERTY_CODIGO_POSTAL)) {
						ciudadFrmDto.setCodigoPostal(ciudadEntity.getCodigoPostal());
					}
					if (ciudadFrmDto.getProvinciaId() == null
							&& !propertiesInJSON.contains(CiudadEdm.PROPERTY_PROVINCIA)) {
						ciudadFrmDto.setProvinciaId(ciudadEntity.getProvincia().getId());
					}
					if (ciudadFrmDto.getPrefijoTelefonico() == null
							&& !propertiesInJSON.contains(CiudadEdm.PROPERTY_PREFIJO)) {
						ciudadFrmDto.setPrefijoTelefonico(ciudadEntity.getPrefijoTelefonico());
					}
				}

				ValidatorUtil.validate(ciudadFrmDtoValidator, messageSource, ciudadFrmDto);
				ciudadEntity = ciudadService.update(ciudadFrmDto);

				return new CiudadEdm(ciudadEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad ciudad.");

	}

	@Override
	public Object delete(Map<String, UriParameter> keyPredicateMap) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(CiudadEdm.PROPERTY_ID).getText());

		try {
			CiudadEntity ciudadEntity = ciudadService.findById(id);
			ciudadService.delete(ciudadEntity);

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}

		return null;
	}

	@Override
	public Object readFromKey(Map<String, UriParameter> keyPredicateMap, ExpandOption expandOption,
			SelectOption selectOption) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(CiudadEdm.PROPERTY_ID).getText());

		try {
			CiudadEntity ciudadEntity = ciudadService.findById(id);
			CiudadEdm ciudadEdm = new CiudadEdm(ciudadEntity);

			return ciudadEdm;

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}
	}

	@Override
	public Iterable<?> readAll(ExpandOption expandOption, FilterOption filterOption, OrderByOption orderByOption)
			throws ODataApplicationException {

		JPQLQuery query = new JPQLQueryBuilder().setDistinct(false).setClazz(CiudadEdm.class)
				.setExpandOption(expandOption).setFilterOption(filterOption).setOrderByOption(orderByOption).build();

		List<CiudadEntity> ciudadEntities = JPQLQuery.execute(entityManager, query);
		List<CiudadEdm> ciudadEdms = ciudadEntities.stream().map(entity -> {
			CiudadEdm ciudadEdm = new CiudadEdm(entity);
			return ciudadEdm;
		}).collect(Collectors.toList());

		return ciudadEdms;
	}

}
