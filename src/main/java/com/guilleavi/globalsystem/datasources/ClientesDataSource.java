package com.guilleavi.globalsystem.datasources;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.OrderByOption;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cairone.olingo.ext.jpa.query.JPQLQuery;
import com.cairone.olingo.ext.jpa.query.JPQLQueryBuilder;
import com.guilleavi.globalsystem.dtos.ClienteFrmDto;
import com.guilleavi.globalsystem.dtos.validators.ClienteFrmDtoValidator;
import com.guilleavi.globalsystem.edm.resources.ClienteEdm;
import com.guilleavi.globalsystem.entities.ClienteEntity;
import com.guilleavi.globalsystem.exceptions.ODataBadRequestException;
import com.guilleavi.globalsystem.services.ClienteService;
import com.guilleavi.globalsystem.utils.OdataExceptionParser;
import com.guilleavi.globalsystem.utils.ValidatorUtil;

@Component
public class ClientesDataSource extends AbstractDataSource {

	@Autowired
	private ClienteService clienteService = null;
	@Autowired
	private ClienteFrmDtoValidator clienteFrmDtoValidator = null;

	@Override
	public String isSuitableFor() {
		return ClienteEdm.ENTITY_SET;
	}

	@Override
	public Object create(Object entity) throws ODataApplicationException {

		if (entity instanceof ClienteEdm) {

			ClienteEdm clienteEdm = (ClienteEdm) entity;

			try {
				ClienteFrmDto clienteFrmDto = new ClienteFrmDto(clienteEdm);

				ValidatorUtil.validate(clienteFrmDtoValidator, messageSource, clienteFrmDto);
				ClienteEntity clienteEntity = clienteService.update(clienteFrmDto);

				return new ClienteEdm(clienteEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad cliente.");

	}

	@Override
	public Object update(Map<String, UriParameter> keyPredicateMap, Object entity, List<String> propertiesInJSON,
			boolean isPut) throws ODataApplicationException {

		if (entity instanceof ClienteEdm) {

			Integer id = Integer.valueOf(keyPredicateMap.get(ClienteEdm.PROPERTY_ID).getText());

			ClienteEdm clienteEdm = (ClienteEdm) entity;

			try {

				ClienteFrmDto clienteFrmDto = new ClienteFrmDto(clienteEdm);

				clienteFrmDto.setId(id);

				ClienteEntity clienteEntity = clienteService.findById(id);

				if (!isPut) {
					if (clienteFrmDto.getPersonaId() == null
							&& !propertiesInJSON.contains(ClienteEdm.PROPERTY_PERSONA)) {
						clienteFrmDto.setPersonaId(clienteEntity.getPersona().getId());
					}
					if (clienteFrmDto.getIvaTipoId() == null
							&& !propertiesInJSON.contains(ClienteEdm.PROPERTY_IVA_TIPO)) {
						clienteFrmDto.setIvaTipoId(clienteEntity.getIvaTipo().getId());
					}
					if (clienteFrmDto.getEstadoId() == null && !propertiesInJSON.contains(ClienteEdm.PROPERTY_ESTADO)) {
						clienteFrmDto.setEstadoId(clienteEntity.getEstado().getId());
					}
				}

				ValidatorUtil.validate(clienteFrmDtoValidator, messageSource, clienteFrmDto);
				clienteEntity = clienteService.update(clienteFrmDto);

				return new ClienteEdm(clienteEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad cliente.");

	}

	@Override
	public Object delete(Map<String, UriParameter> keyPredicateMap) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(ClienteEdm.PROPERTY_ID).getText());

		try {
			ClienteEntity clienteEntity = clienteService.findById(id);
			clienteService.delete(clienteEntity);

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}

		return null;
	}

	@Override
	public Object readFromKey(Map<String, UriParameter> keyPredicateMap, ExpandOption expandOption,
			SelectOption selectOption) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(ClienteEdm.PROPERTY_ID).getText());

		try {
			ClienteEntity clienteEntity = clienteService.findById(id);
			ClienteEdm clienteEdm = new ClienteEdm(clienteEntity);

			return clienteEdm;

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}
	}

	@Override
	public Iterable<?> readAll(ExpandOption expandOption, FilterOption filterOption, OrderByOption orderByOption)
			throws ODataApplicationException {

		JPQLQuery query = new JPQLQueryBuilder().setDistinct(false).setClazz(ClienteEdm.class)
				.setExpandOption(expandOption).setFilterOption(filterOption).setOrderByOption(orderByOption).build();

		List<ClienteEntity> clienteEntities = JPQLQuery.execute(entityManager, query);
		List<ClienteEdm> clienteEdms = clienteEntities.stream().map(entity -> {
			ClienteEdm clienteEdm = new ClienteEdm(entity);
			return clienteEdm;
		}).collect(Collectors.toList());

		return clienteEdms;
	}
}
