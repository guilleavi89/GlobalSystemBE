package com.guilleavi.globalsystem.datasources;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.OrderByOption;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cairone.olingo.ext.jpa.query.JPQLQuery;
import com.cairone.olingo.ext.jpa.query.JPQLQueryBuilder;
import com.guilleavi.globalsystem.dtos.PaisFrmDto;
import com.guilleavi.globalsystem.dtos.validators.PaisFrmDtoValidator;
import com.guilleavi.globalsystem.edm.resources.PaisEdm;
import com.guilleavi.globalsystem.entities.PaisEntity;
import com.guilleavi.globalsystem.exceptions.ODataBadRequestException;
import com.guilleavi.globalsystem.services.PaisService;
import com.guilleavi.globalsystem.utils.OdataExceptionParser;
import com.guilleavi.globalsystem.utils.ValidatorUtil;

@Component
public class PaisesDataSource extends AbstractDataSource {

	@Autowired
	private PaisService paisService = null;
	@Autowired
	private PaisFrmDtoValidator paisFrmDtoValidator = null;

	@Override
	public String isSuitableFor() {
		return PaisEdm.ENTITY_SET;
	}

	@Override
	public Object create(Object entity) throws ODataApplicationException {

		if (entity instanceof PaisEdm) {

			PaisEdm paisEdm = (PaisEdm) entity;

			try {
				PaisFrmDto paisFrmDto = new PaisFrmDto(paisEdm);

				ValidatorUtil.validate(paisFrmDtoValidator, messageSource, paisFrmDto);
				PaisEntity paisEntity = paisService.update(paisFrmDto);

				return new PaisEdm(paisEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad pais.");

	}

	@Override
	public Object update(Map<String, UriParameter> keyPredicateMap, Object entity, List<String> propertiesInJSON,
			boolean isPut) throws ODataApplicationException {

		if (entity instanceof PaisEdm) {

			Integer id = Integer.valueOf(keyPredicateMap.get(PaisEdm.PROPERTY_ID).getText());

			PaisEdm paisEdm = (PaisEdm) entity;

			try {

				PaisFrmDto paisFrmDto = new PaisFrmDto(paisEdm);

				paisFrmDto.setId(id);

				PaisEntity paisEntity = paisService.findById(id);

				if (!isPut) {
					if (paisFrmDto.getNombre() == null && !propertiesInJSON.contains(PaisEdm.PROPERTY_NOMBRE)) {
						paisFrmDto.setNombre(paisEntity.getNombre());
					}
					if (paisFrmDto.getPrefijoTelefonico() == null
							&& !propertiesInJSON.contains(PaisEdm.PROPERTY_PREFIJO)) {
						paisFrmDto.setPrefijoTelefonico(paisEntity.getPrefijoTelefonico());
					}
				}

				ValidatorUtil.validate(paisFrmDtoValidator, messageSource, paisFrmDto);
				paisEntity = paisService.update(paisFrmDto);

				return new PaisEdm(paisEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad pais.");

	}

	@Override
	public Object delete(Map<String, UriParameter> keyPredicateMap) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(PaisEdm.PROPERTY_ID).getText());

		try {
			PaisEntity paisEntity = paisService.findById(id);
			paisService.delete(paisEntity);

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}

		return null;
	}

	@Override
	public Object readFromKey(Map<String, UriParameter> keyPredicateMap, ExpandOption expandOption,
			SelectOption selectOption) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(PaisEdm.PROPERTY_ID).getText());

		try {
			PaisEntity paisEntity = paisService.findById(id);
			PaisEdm paisEdm = new PaisEdm(paisEntity);

			return paisEdm;

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}
	}

	@Override
	public Iterable<?> readAll(ExpandOption expandOption, FilterOption filterOption, OrderByOption orderByOption)
			throws ODataApplicationException {

		JPQLQuery query = new JPQLQueryBuilder().setDistinct(false).setClazz(PaisEdm.class)
				.setExpandOption(expandOption).setFilterOption(filterOption).setOrderByOption(orderByOption).build();

		List<PaisEntity> paisEntities = JPQLQuery.execute(entityManager, query);
		List<PaisEdm> paisEdms = paisEntities.stream().map(entity -> {
			PaisEdm paisEdm = new PaisEdm(entity);
			return paisEdm;
		}).collect(Collectors.toList());

		return paisEdms;
	}
}
