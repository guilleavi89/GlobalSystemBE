package com.guilleavi.globalsystem.datasources;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.OrderByOption;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cairone.olingo.ext.jpa.query.JPQLQuery;
import com.cairone.olingo.ext.jpa.query.JPQLQueryBuilder;
import com.guilleavi.globalsystem.dtos.ProvinciaFrmDto;
import com.guilleavi.globalsystem.dtos.validators.ProvinciaFrmDtoValidator;
import com.guilleavi.globalsystem.edm.resources.ProvinciaEdm;
import com.guilleavi.globalsystem.entities.ProvinciaEntity;
import com.guilleavi.globalsystem.exceptions.ODataBadRequestException;
import com.guilleavi.globalsystem.services.ProvinciaService;
import com.guilleavi.globalsystem.utils.OdataExceptionParser;
import com.guilleavi.globalsystem.utils.ValidatorUtil;

@Component
public class ProvinciasDataSource extends AbstractDataSource {

	@Autowired
	private ProvinciaService provinciaService = null;
	@Autowired
	private ProvinciaFrmDtoValidator provinciaFrmDtoValidator = null;

	@Override
	public String isSuitableFor() {
		return ProvinciaEdm.ENTITY_SET;
	}

	@Override
	public Object create(Object entity) throws ODataApplicationException {

		if (entity instanceof ProvinciaEdm) {

			ProvinciaEdm provinciaEdm = (ProvinciaEdm) entity;

			try {
				ProvinciaFrmDto provinciaFrmDto = new ProvinciaFrmDto(provinciaEdm);

				ValidatorUtil.validate(provinciaFrmDtoValidator, messageSource, provinciaFrmDto);
				ProvinciaEntity provinciaEntity = provinciaService.update(provinciaFrmDto);

				return new ProvinciaEdm(provinciaEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad provincia.");

	}

	@Override
	public Object update(Map<String, UriParameter> keyPredicateMap, Object entity, List<String> propertiesInJSON,
			boolean isPut) throws ODataApplicationException {

		if (entity instanceof ProvinciaEdm) {

			Integer id = Integer.valueOf(keyPredicateMap.get(ProvinciaEdm.PROPERTY_ID).getText());

			ProvinciaEdm provinciaEdm = (ProvinciaEdm) entity;

			try {

				ProvinciaFrmDto provinciaFrmDto = new ProvinciaFrmDto(provinciaEdm);

				provinciaFrmDto.setId(id);

				ProvinciaEntity provinciaEntity = provinciaService.findById(id);

				if (!isPut) {
					if (provinciaFrmDto.getNombre() == null
							&& !propertiesInJSON.contains(ProvinciaEdm.PROPERTY_NOMBRE)) {
						provinciaFrmDto.setNombre(provinciaEntity.getNombre());
					}
					if (provinciaFrmDto.getPaisId() == null && !propertiesInJSON.contains(ProvinciaEdm.PROPERTY_PAIS)) {
						provinciaFrmDto.setPaisId(provinciaEntity.getPais().getId());
					}
				}

				ValidatorUtil.validate(provinciaFrmDtoValidator, messageSource, provinciaFrmDto);
				provinciaEntity = provinciaService.update(provinciaFrmDto);

				return new ProvinciaEdm(provinciaEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad provincia.");

	}

	@Override
	public Object delete(Map<String, UriParameter> keyPredicateMap) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(ProvinciaEdm.PROPERTY_ID).getText());

		try {
			ProvinciaEntity provinciaEntity = provinciaService.findById(id);
			provinciaService.delete(provinciaEntity);

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}

		return null;
	}

	@Override
	public Object readFromKey(Map<String, UriParameter> keyPredicateMap, ExpandOption expandOption,
			SelectOption selectOption) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(ProvinciaEdm.PROPERTY_ID).getText());

		try {
			ProvinciaEntity provinciaEntity = provinciaService.findById(id);
			ProvinciaEdm provinciaEdm = new ProvinciaEdm(provinciaEntity);

			return provinciaEdm;

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}
	}

	@Override
	public Iterable<?> readAll(ExpandOption expandOption, FilterOption filterOption, OrderByOption orderByOption)
			throws ODataApplicationException {

		JPQLQuery query = new JPQLQueryBuilder().setDistinct(false).setClazz(ProvinciaEdm.class)
				.setExpandOption(expandOption).setFilterOption(filterOption).setOrderByOption(orderByOption).build();

		List<ProvinciaEntity> provinciaEntities = JPQLQuery.execute(entityManager, query);
		List<ProvinciaEdm> provinciaEdms = provinciaEntities.stream().map(entity -> {
			ProvinciaEdm provinciaEdm = new ProvinciaEdm(entity);
			return provinciaEdm;
		}).collect(Collectors.toList());

		return provinciaEdms;
	}
}