package com.guilleavi.globalsystem.datasources;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.OrderByOption;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cairone.olingo.ext.jpa.query.JPQLQuery;
import com.cairone.olingo.ext.jpa.query.JPQLQueryBuilder;
import com.guilleavi.globalsystem.dtos.PersonaFrmDto;
import com.guilleavi.globalsystem.dtos.validators.PersonaFrmDtoValidator;
import com.guilleavi.globalsystem.edm.resources.PersonaEdm;
import com.guilleavi.globalsystem.entities.PersonaEntity;
import com.guilleavi.globalsystem.exceptions.ODataBadRequestException;
import com.guilleavi.globalsystem.services.PersonaService;
import com.guilleavi.globalsystem.utils.OdataExceptionParser;
import com.guilleavi.globalsystem.utils.ValidatorUtil;

@Component
public class PersonasDataSource extends AbstractDataSource {

	@Autowired
	private PersonaService personaService = null;
	@Autowired
	private PersonaFrmDtoValidator personaFrmDtoValidator = null;

	@Override
	public String isSuitableFor() {
		return PersonaEdm.ENTITY_SET;
	}

	@Override
	public Object create(Object entity) throws ODataApplicationException {

		if (entity instanceof PersonaEdm) {

			PersonaEdm personaEdm = (PersonaEdm) entity;

			try {
				PersonaFrmDto personaFrmDto = new PersonaFrmDto(personaEdm);

				ValidatorUtil.validate(personaFrmDtoValidator, messageSource, personaFrmDto);
				PersonaEntity personaEntity = personaService.update(personaFrmDto);

				return new PersonaEdm(personaEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad persona.");

	}

	@Override
	public Object update(Map<String, UriParameter> keyPredicateMap, Object entity, List<String> propertiesInJSON,
			boolean isPut) throws ODataApplicationException {

		if (entity instanceof PersonaEdm) {

			Integer id = Integer.valueOf(keyPredicateMap.get(PersonaEdm.PROPERTY_ID).getText());

			PersonaEdm personaEdm = (PersonaEdm) entity;

			try {

				PersonaFrmDto personaFrmDto = new PersonaFrmDto(personaEdm);

				personaFrmDto.setId(id);

				PersonaEntity personaEntity = personaService.findById(id);

				if (!isPut) {
					if (personaFrmDto.getNombre() == null && !propertiesInJSON.contains(PersonaEdm.PROPERTY_NOMBRE)) {
						personaFrmDto.setNombre(personaEntity.getNombre());
					}
					if (personaFrmDto.getTipoId() == null && !propertiesInJSON.contains(PersonaEdm.PROPERTY_TIPO)) {
						personaFrmDto.setTipoId(personaEntity.getTipo().getId());
					}
					if (personaFrmDto.getDocumentoTipoId() == null
							&& !propertiesInJSON.contains(PersonaEdm.PROPERTY_DOCUMENTO_TIPO)) {
						personaFrmDto.setDocumentoTipoId(personaEntity.getDocumentoTipo().getId());
					}
					if (personaFrmDto.getDocumentoNumero() == null
							&& !propertiesInJSON.contains(PersonaEdm.PROPERTY_DOCUMENTO_NUMERO)) {
						personaFrmDto.setDocumentoNumero(personaEntity.getDocumentoNumero());
					}
				}

				ValidatorUtil.validate(personaFrmDtoValidator, messageSource, personaFrmDto);
				personaEntity = personaService.update(personaFrmDto);

				return new PersonaEdm(personaEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad persona.");

	}

	@Override
	public Object delete(Map<String, UriParameter> keyPredicateMap) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(PersonaEdm.PROPERTY_ID).getText());

		try {
			PersonaEntity personaEntity = personaService.findById(id);
			personaService.delete(personaEntity);

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}

		return null;
	}

	@Override
	public Object readFromKey(Map<String, UriParameter> keyPredicateMap, ExpandOption expandOption,
			SelectOption selectOption) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(PersonaEdm.PROPERTY_ID).getText());

		try {
			PersonaEntity personaEntity = personaService.findById(id);
			PersonaEdm personaEdm = new PersonaEdm(personaEntity);

			return personaEdm;

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}
	}

	@Override
	public Iterable<?> readAll(ExpandOption expandOption, FilterOption filterOption, OrderByOption orderByOption)
			throws ODataApplicationException {

		JPQLQuery query = new JPQLQueryBuilder().setDistinct(false).setClazz(PersonaEdm.class)
				.setExpandOption(expandOption).setFilterOption(filterOption).setOrderByOption(orderByOption).build();

		List<PersonaEntity> personaEntities = JPQLQuery.execute(entityManager, query);
		List<PersonaEdm> personaEdms = personaEntities.stream().map(entity -> {
			PersonaEdm personaEdm = new PersonaEdm(entity);
			return personaEdm;
		}).collect(Collectors.toList());

		return personaEdms;
	}
}
