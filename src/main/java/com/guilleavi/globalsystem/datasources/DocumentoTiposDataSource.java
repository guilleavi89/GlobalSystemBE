package com.guilleavi.globalsystem.datasources;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.OrderByOption;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cairone.olingo.ext.jpa.query.JPQLQuery;
import com.cairone.olingo.ext.jpa.query.JPQLQueryBuilder;
import com.guilleavi.globalsystem.dtos.DocumentoTipoFrmDto;
import com.guilleavi.globalsystem.dtos.validators.DocumentoTipoFrmDtoValidator;
import com.guilleavi.globalsystem.edm.resources.DocumentoTipoEdm;
import com.guilleavi.globalsystem.entities.DocumentoTipoEntity;
import com.guilleavi.globalsystem.exceptions.ODataBadRequestException;
import com.guilleavi.globalsystem.services.DocumentoTipoService;
import com.guilleavi.globalsystem.utils.OdataExceptionParser;
import com.guilleavi.globalsystem.utils.ValidatorUtil;

@Component
public class DocumentoTiposDataSource extends AbstractDataSource {

	@Autowired
	private DocumentoTipoService documentoTipoService = null;
	@Autowired
	private DocumentoTipoFrmDtoValidator documentoTipoFrmDtoValidator = null;

	@Override
	public String isSuitableFor() {
		return DocumentoTipoEdm.ENTITY_SET;
	}

	@Override
	public Object create(Object entity) throws ODataApplicationException {

		if (entity instanceof DocumentoTipoEdm) {

			DocumentoTipoEdm documentoTipoEdm = (DocumentoTipoEdm) entity;

			try {
				DocumentoTipoFrmDto documentoTipoFrmDto = new DocumentoTipoFrmDto(documentoTipoEdm);

				ValidatorUtil.validate(documentoTipoFrmDtoValidator, messageSource, documentoTipoFrmDto);
				DocumentoTipoEntity documentoTipoEntity = documentoTipoService.update(documentoTipoFrmDto);

				return new DocumentoTipoEdm(documentoTipoEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad tipo de documento.");

	}

	@Override
	public Object update(Map<String, UriParameter> keyPredicateMap, Object entity, List<String> propertiesInJSON,
			boolean isPut) throws ODataApplicationException {

		if (entity instanceof DocumentoTipoEdm) {

			Integer id = Integer.valueOf(keyPredicateMap.get(DocumentoTipoEdm.PROPERTY_ID).getText());

			DocumentoTipoEdm documentoTipoEdm = (DocumentoTipoEdm) entity;

			try {

				DocumentoTipoFrmDto documentoTipoFrmDto = new DocumentoTipoFrmDto(documentoTipoEdm);

				documentoTipoFrmDto.setId(id);

				DocumentoTipoEntity documentoTipoEntity = documentoTipoService.findById(id);

				if (!isPut) {
					if (documentoTipoFrmDto.getDescripcion() == null
							&& !propertiesInJSON.contains(DocumentoTipoEdm.PROPERTY_DESCRIPCION)) {
						documentoTipoFrmDto.setDescripcion(documentoTipoEntity.getDescripcion());
					}
					if (documentoTipoFrmDto.getDescripcionReducida() == null
							&& !propertiesInJSON.contains(DocumentoTipoEdm.PROPERTY_DESCRIPCION_REDUCIDA)) {
						documentoTipoFrmDto.setDescripcionReducida(documentoTipoEntity.getDescripcionReducida());
					}
				}

				ValidatorUtil.validate(documentoTipoFrmDtoValidator, messageSource, documentoTipoFrmDto);
				documentoTipoEntity = documentoTipoService.update(documentoTipoFrmDto);

				return new DocumentoTipoEdm(documentoTipoEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad tipo de documento.");

	}

	@Override
	public Object delete(Map<String, UriParameter> keyPredicateMap) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(DocumentoTipoEdm.PROPERTY_ID).getText());

		try {
			DocumentoTipoEntity documentoTipoEntity = documentoTipoService.findById(id);
			documentoTipoService.delete(documentoTipoEntity);

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}

		return null;
	}

	@Override
	public Object readFromKey(Map<String, UriParameter> keyPredicateMap, ExpandOption expandOption,
			SelectOption selectOption) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(DocumentoTipoEdm.PROPERTY_ID).getText());

		try {
			DocumentoTipoEntity documentoTipoEntity = documentoTipoService.findById(id);
			DocumentoTipoEdm documentoTipoEdm = new DocumentoTipoEdm(documentoTipoEntity);

			return documentoTipoEdm;

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}
	}

	@Override
	public Iterable<?> readAll(ExpandOption expandOption, FilterOption filterOption, OrderByOption orderByOption)
			throws ODataApplicationException {

		JPQLQuery query = new JPQLQueryBuilder().setDistinct(false).setClazz(DocumentoTipoEdm.class)
				.setExpandOption(expandOption).setFilterOption(filterOption).setOrderByOption(orderByOption).build();

		List<DocumentoTipoEntity> documentoTipoEntities = JPQLQuery.execute(entityManager, query);
		List<DocumentoTipoEdm> documentoTipoEdms = documentoTipoEntities.stream().map(entity -> {
			DocumentoTipoEdm documentoTipoEdm = new DocumentoTipoEdm(entity);
			return documentoTipoEdm;
		}).collect(Collectors.toList());

		return documentoTipoEdms;
	}
}
