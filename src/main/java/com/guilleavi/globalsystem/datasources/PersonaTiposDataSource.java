package com.guilleavi.globalsystem.datasources;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.apache.olingo.server.api.ODataApplicationException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.apache.olingo.server.api.uri.queryoption.ExpandOption;
import org.apache.olingo.server.api.uri.queryoption.FilterOption;
import org.apache.olingo.server.api.uri.queryoption.OrderByOption;
import org.apache.olingo.server.api.uri.queryoption.SelectOption;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cairone.olingo.ext.jpa.query.JPQLQuery;
import com.cairone.olingo.ext.jpa.query.JPQLQueryBuilder;
import com.guilleavi.globalsystem.dtos.PersonaTipoFrmDto;
import com.guilleavi.globalsystem.dtos.validators.PersonaTipoFrmDtoValidator;
import com.guilleavi.globalsystem.edm.resources.PersonaTipoEdm;
import com.guilleavi.globalsystem.entities.PersonaTipoEntity;
import com.guilleavi.globalsystem.exceptions.ODataBadRequestException;
import com.guilleavi.globalsystem.services.PersonaTipoService;
import com.guilleavi.globalsystem.utils.OdataExceptionParser;
import com.guilleavi.globalsystem.utils.ValidatorUtil;

@Component
public class PersonaTiposDataSource extends AbstractDataSource {

	@Autowired
	private PersonaTipoService personaTipoService = null;
	@Autowired
	private PersonaTipoFrmDtoValidator personaTipoFrmDtoValidator = null;

	@Override
	public String isSuitableFor() {
		return PersonaTipoEdm.ENTITY_SET;
	}

	@Override
	public Object create(Object entity) throws ODataApplicationException {

		if (entity instanceof PersonaTipoEdm) {

			PersonaTipoEdm personaTipoEdm = (PersonaTipoEdm) entity;

			try {
				PersonaTipoFrmDto personaTipoFrmDto = new PersonaTipoFrmDto(personaTipoEdm);

				ValidatorUtil.validate(personaTipoFrmDtoValidator, messageSource, personaTipoFrmDto);
				PersonaTipoEntity personaTipoEntity = personaTipoService.update(personaTipoFrmDto);

				return new PersonaTipoEdm(personaTipoEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad  tipo de persona.");

	}

	@Override
	public Object update(Map<String, UriParameter> keyPredicateMap, Object entity, List<String> propertiesInJSON,
			boolean isPut) throws ODataApplicationException {

		if (entity instanceof PersonaTipoEdm) {

			Integer id = Integer.valueOf(keyPredicateMap.get(PersonaTipoEdm.PROPERTY_ID).getText());

			PersonaTipoEdm personaTipoEdm = (PersonaTipoEdm) entity;

			try {

				PersonaTipoFrmDto personaTipoFrmDto = new PersonaTipoFrmDto(personaTipoEdm);

				personaTipoFrmDto.setId(id);

				PersonaTipoEntity personaTipoEntity = personaTipoService.findById(id);

				if (!isPut) {
					if (personaTipoFrmDto.getDescripcion() == null
							&& !propertiesInJSON.contains(PersonaTipoEdm.PROPERTY_DESCRIPCION)) {
						personaTipoFrmDto.setDescripcion(personaTipoEntity.getDescripcion());
					}
				}

				ValidatorUtil.validate(personaTipoFrmDtoValidator, messageSource, personaTipoFrmDto);
				personaTipoEntity = personaTipoService.update(personaTipoFrmDto);

				return new PersonaTipoEdm(personaTipoEntity);

			} catch (Exception e) {
				throw OdataExceptionParser.parse(e);
			}
		}

		throw new ODataBadRequestException("Los datos no corresponden a la entidad tipo de persona.");

	}

	@Override
	public Object delete(Map<String, UriParameter> keyPredicateMap) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(PersonaTipoEdm.PROPERTY_ID).getText());

		try {
			PersonaTipoEntity personaTipoEntity = personaTipoService.findById(id);
			personaTipoService.delete(personaTipoEntity);

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}

		return null;
	}

	@Override
	public Object readFromKey(Map<String, UriParameter> keyPredicateMap, ExpandOption expandOption,
			SelectOption selectOption) throws ODataApplicationException {

		Integer id = Integer.valueOf(keyPredicateMap.get(PersonaTipoEdm.PROPERTY_ID).getText());

		try {
			PersonaTipoEntity personaTipoEntity = personaTipoService.findById(id);
			PersonaTipoEdm personaTipoEdm = new PersonaTipoEdm(personaTipoEntity);

			return personaTipoEdm;

		} catch (Exception e) {
			throw OdataExceptionParser.parse(e);
		}
	}

	@Override
	public Iterable<?> readAll(ExpandOption expandOption, FilterOption filterOption, OrderByOption orderByOption)
			throws ODataApplicationException {

		JPQLQuery query = new JPQLQueryBuilder().setDistinct(false).setClazz(PersonaTipoEdm.class)
				.setExpandOption(expandOption).setFilterOption(filterOption).setOrderByOption(orderByOption).build();

		List<PersonaTipoEntity> personaTipoEntities = JPQLQuery.execute(entityManager, query);
		List<PersonaTipoEdm> personaTipoEdms = personaTipoEntities.stream().map(entity -> {
			PersonaTipoEdm personaTipoEdm = new PersonaTipoEdm(entity);
			return personaTipoEdm;
		}).collect(Collectors.toList());

		return personaTipoEdms;
	}

}
