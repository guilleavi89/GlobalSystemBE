package com.guilleavi.globalsystem.edm.resources;

import com.cairone.olingo.ext.jpa.annotations.EdmEntity;
import com.cairone.olingo.ext.jpa.annotations.EdmEntitySet;
import com.cairone.olingo.ext.jpa.annotations.EdmNavigationProperty;
import com.cairone.olingo.ext.jpa.annotations.EdmProperty;
import com.cairone.olingo.ext.jpa.annotations.ODataJPAEntity;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.entities.CiudadEntity;

@EdmEntity(name = CiudadEdm.ENTITY_NAME, key = {
		CiudadEdm.PROPERTY_ID }, namespace = AppConstants.NAME_SPACE, containerName = AppConstants.CONTAINER_NAME)
@EdmEntitySet(CiudadEdm.ENTITY_SET)
@ODataJPAEntity(CiudadEdm.ENTITY)
public class CiudadEdm {

	public static final String ENTITY_NAME = "Ciudad";
	public static final String ENTITY_SET = "Ciudades";
	public static final String ENTITY = "CiudadEntity";
	public static final String PROPERTY_ID = "Id";
	public static final String PROPERTY_NOMBRE = "Nombre";
	public static final String PROPERTY_CODIGO_POSTAL = "CodigoPostal";
	public static final String PROPERTY_PROVINCIA = "Provincia";
	public static final String PROPERTY_PREFIJO = "PrefijoTelefonico";

	@EdmProperty(name = PROPERTY_ID, nullable = false)
	private Integer id = null;

	@EdmProperty(name = PROPERTY_NOMBRE, nullable = false, maxLength = 100)
	private String nombre = null;

	@EdmProperty(name = PROPERTY_CODIGO_POSTAL, nullable = false, maxLength = 50)
	private String codigoPostal = null;

	@EdmNavigationProperty(name = PROPERTY_PROVINCIA, nullable = false)
	private ProvinciaEdm provincia = null;

	@EdmProperty(name = PROPERTY_PREFIJO, nullable = true, maxLength = 5)
	private String prefijoTelefonico = null;

	public CiudadEdm() {
		super();
	}

	public CiudadEdm(Integer id, String nombre, String codigoPostal, ProvinciaEdm provincia, String prefijoTelefonico) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.codigoPostal = codigoPostal;
		this.provincia = provincia;
		this.prefijoTelefonico = prefijoTelefonico;
	}

	public CiudadEdm(CiudadEntity ciudadEntity) {
		this(ciudadEntity.getId(), ciudadEntity.getNombre(), ciudadEntity.getCodigoPostal(),
				new ProvinciaEdm(ciudadEntity.getProvincia()), ciudadEntity.getPrefijoTelefonico());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getCodigoPostal() {
		return codigoPostal;
	}

	public void setCodigoPostal(String codigoPostal) {
		this.codigoPostal = codigoPostal;
	}

	public ProvinciaEdm getProvincia() {
		return provincia;
	}

	public void setProvincia(ProvinciaEdm provincia) {
		this.provincia = provincia;
	}

	public String getPrefijoTelefonico() {
		return prefijoTelefonico;
	}

	public void setPrefijoTelefonico(String prefijoTelefonico) {
		this.prefijoTelefonico = prefijoTelefonico;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		CiudadEdm other = (CiudadEdm) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "CiudadEdm [id=" + id + ", nombre=" + nombre + "]";
	}

}
