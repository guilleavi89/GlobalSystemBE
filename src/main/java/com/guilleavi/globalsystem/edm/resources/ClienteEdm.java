package com.guilleavi.globalsystem.edm.resources;

import com.cairone.olingo.ext.jpa.annotations.EdmEntity;
import com.cairone.olingo.ext.jpa.annotations.EdmEntitySet;
import com.cairone.olingo.ext.jpa.annotations.EdmNavigationProperty;
import com.cairone.olingo.ext.jpa.annotations.EdmProperty;
import com.cairone.olingo.ext.jpa.annotations.ODataJPAEntity;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.entities.ClienteEntity;

@EdmEntity(name = ClienteEdm.EDM_NAME, key = {
		ClienteEdm.PROPERTY_ID }, namespace = AppConstants.NAME_SPACE, containerName = AppConstants.CONTAINER_NAME)
@EdmEntitySet(ClienteEdm.ENTITY_SET)
@ODataJPAEntity(ClienteEdm.ENTITY)
public class ClienteEdm {

	public static final String EDM_NAME = "Cliente";
	public static final String ENTITY_SET = "Clientes";
	public static final String ENTITY = "ClienteEntity";
	public static final String PROPERTY_ID = "Id";
	public static final String PROPERTY_PERSONA = "Persona";
	public static final String PROPERTY_IVA_TIPO = "IvaTipo";
	public static final String PROPERTY_ESTADO = "Estado";

	@EdmProperty(name = PROPERTY_ID, nullable = false)
	private Integer id = null;

	@EdmNavigationProperty(name = PROPERTY_PERSONA, nullable = false)
	private PersonaEdm persona = null;

	@EdmNavigationProperty(name = PROPERTY_IVA_TIPO, nullable = false)
	private IvaTipoEdm ivaTipo = null;

	@EdmNavigationProperty(name = PROPERTY_ESTADO, nullable = false)
	private EstadoEdm estado = null;

	public ClienteEdm() {
		super();
	}

	public ClienteEdm(Integer id, PersonaEdm persona, IvaTipoEdm ivaTipo, EstadoEdm estado) {
		super();
		this.id = id;
		this.persona = persona;
		this.ivaTipo = ivaTipo;
		this.estado = estado;
	}

	public ClienteEdm(ClienteEntity clienteEntity) {
		this(clienteEntity.getId(), new PersonaEdm(clienteEntity.getPersona()),
				new IvaTipoEdm(clienteEntity.getIvaTipo()), new EstadoEdm(clienteEntity.getEstado()));
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PersonaEdm getPersona() {
		return persona;
	}

	public void setPersona(PersonaEdm persona) {
		this.persona = persona;
	}

	public IvaTipoEdm getIvaTipo() {
		return ivaTipo;
	}

	public void setIvaTipo(IvaTipoEdm ivaTipo) {
		this.ivaTipo = ivaTipo;
	}

	public EstadoEdm getEstado() {
		return estado;
	}

	public void setEstado(EstadoEdm estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ClienteEdm other = (ClienteEdm) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ClienteEdm [id=" + id + "]";
	}
}
