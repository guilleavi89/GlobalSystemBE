package com.guilleavi.globalsystem.edm.resources;

import com.cairone.olingo.ext.jpa.annotations.EdmEntity;
import com.cairone.olingo.ext.jpa.annotations.EdmEntitySet;
import com.cairone.olingo.ext.jpa.annotations.EdmProperty;
import com.cairone.olingo.ext.jpa.annotations.ODataJPAEntity;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.entities.PersonEntity;

@EdmEntity(name = PersonEdm.ENTITY_NAME, key = {
		PersonEdm.PROPERTY_ID }, namespace = AppConstants.NAME_SPACE, containerName = AppConstants.CONTAINER_NAME)
@EdmEntitySet(PersonEdm.ENTITY_SET)
@ODataJPAEntity(PersonEdm.ENTITY)
public class PersonEdm {

	public static final String ENTITY_NAME = "Person";
	public static final String PROPERTY_ID = "Id";
	public static final String PROPERTY_NAME = "Name";
	public static final String PROPERTY_LASTNAME = "Lastname";
	public static final String ENTITY_SET = "People";
	public static final String ENTITY = "PersonEntity";

	@EdmProperty(name = PROPERTY_ID, nullable = false)
	private Integer id = null;

	@EdmProperty(name = PROPERTY_LASTNAME, nullable = false, maxLength = 100)
	private String lastname = null;

	@EdmProperty(name = PROPERTY_NAME, nullable = false, maxLength = 100)
	private String name = null;

	public PersonEdm() {
		super();
	}

	public PersonEdm(Integer id, String lastname, String name) {
		super();
		this.id = id;
		this.lastname = lastname;
		this.name = name;
	}

	public PersonEdm(PersonEntity person) {
		this(person.getId(), person.getLastname(), person.getName());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getLastname() {
		return lastname;
	}

	public void setLastname(String lastname) {
		this.lastname = lastname;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PersonEdm other = (PersonEdm) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PersonEdm [id=" + id + ", lastname=" + lastname + ", name=" + name + "]";
	}

}
