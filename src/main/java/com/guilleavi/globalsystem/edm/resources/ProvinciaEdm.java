package com.guilleavi.globalsystem.edm.resources;

import com.cairone.olingo.ext.jpa.annotations.EdmEntity;
import com.cairone.olingo.ext.jpa.annotations.EdmEntitySet;
import com.cairone.olingo.ext.jpa.annotations.EdmNavigationProperty;
import com.cairone.olingo.ext.jpa.annotations.EdmProperty;
import com.cairone.olingo.ext.jpa.annotations.ODataJPAEntity;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.entities.ProvinciaEntity;

@EdmEntity(name = ProvinciaEdm.ENTITY_NAME, key = {
		ProvinciaEdm.PROPERTY_ID }, namespace = AppConstants.NAME_SPACE, containerName = AppConstants.CONTAINER_NAME)
@EdmEntitySet(ProvinciaEdm.ENTITY_SET)
@ODataJPAEntity(ProvinciaEdm.ENTITY)
public class ProvinciaEdm {

	public static final String ENTITY_NAME = "Provincia";
	public static final String ENTITY_SET = "Provincias";
	public static final String ENTITY = "ProvinciaEntity";
	public static final String PROPERTY_ID = "Id";
	public static final String PROPERTY_NOMBRE = "Nombre";
	public static final String PROPERTY_PAIS = "Pais";

	@EdmProperty(name = PROPERTY_ID, nullable = false)
	private Integer id = null;

	@EdmProperty(name = PROPERTY_NOMBRE, nullable = false, maxLength = 100)
	private String nombre = null;

	@EdmNavigationProperty(name = PROPERTY_PAIS, nullable = false)
	private PaisEdm pais = null;

	public ProvinciaEdm() {
		super();
	}

	public ProvinciaEdm(Integer id, String nombre, PaisEdm pais) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.pais = pais;
	}

	public ProvinciaEdm(ProvinciaEntity provinciaEntity) {
		this(provinciaEntity.getId(), provinciaEntity.getNombre(), new PaisEdm(provinciaEntity.getPais()));
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public PaisEdm getPais() {
		return pais;
	}

	public void setPais(PaisEdm pais) {
		this.pais = pais;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		ProvinciaEdm other = (ProvinciaEdm) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ProvinciaEdm [id=" + id + ", nombre=" + nombre + "]";
	}
}
