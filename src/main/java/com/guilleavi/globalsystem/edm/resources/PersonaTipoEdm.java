package com.guilleavi.globalsystem.edm.resources;

import com.cairone.olingo.ext.jpa.annotations.EdmEntity;
import com.cairone.olingo.ext.jpa.annotations.EdmEntitySet;
import com.cairone.olingo.ext.jpa.annotations.EdmProperty;
import com.cairone.olingo.ext.jpa.annotations.ODataJPAEntity;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.entities.PersonaTipoEntity;

@EdmEntity(name = PersonaTipoEdm.ENTITY_NAME, key = {
		PersonaTipoEdm.PROPERTY_ID }, namespace = AppConstants.NAME_SPACE, containerName = AppConstants.CONTAINER_NAME)
@EdmEntitySet(PersonaTipoEdm.ENTITY_SET)
@ODataJPAEntity(PersonaTipoEdm.ENTITY)
public class PersonaTipoEdm {

	public static final String ENTITY_NAME = "PersonaTipo";
	public static final String ENTITY_SET = "PersonaTipos";
	public static final String ENTITY = "PersonaTipoEntity";
	public static final String PROPERTY_ID = "Id";
	public static final String PROPERTY_DESCRIPCION = "Descripcion";

	@EdmProperty(name = PROPERTY_ID, nullable = false)
	private Integer id = null;

	@EdmProperty(name = PROPERTY_DESCRIPCION, nullable = false, maxLength = 100)
	private String descripcion = null;

	public PersonaTipoEdm() {
		super();
	}

	public PersonaTipoEdm(Integer id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public PersonaTipoEdm(PersonaTipoEntity personaTipoEntity) {
		this(personaTipoEntity.getId(), personaTipoEntity.getDescripcion());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PersonaTipoEdm other = (PersonaTipoEdm) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PersonaTipoEdm [id=" + id + ", descripcion=" + descripcion + "]";
	}

}
