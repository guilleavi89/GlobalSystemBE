package com.guilleavi.globalsystem.edm.resources;

import com.cairone.olingo.ext.jpa.annotations.EdmEntity;
import com.cairone.olingo.ext.jpa.annotations.EdmEntitySet;
import com.cairone.olingo.ext.jpa.annotations.EdmProperty;
import com.cairone.olingo.ext.jpa.annotations.ODataJPAEntity;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.entities.IvaTipoEntity;

@EdmEntity(name = IvaTipoEdm.ENTITY_NAME, key = {
		IvaTipoEdm.PROPERTY_ID }, namespace = AppConstants.NAME_SPACE, containerName = AppConstants.CONTAINER_NAME)
@EdmEntitySet(IvaTipoEdm.ENTITY_SET)
@ODataJPAEntity(IvaTipoEdm.ENTITY)
public class IvaTipoEdm {

	public static final String ENTITY_NAME = "IvaTipo";
	public static final String ENTITY_SET = "IvaTipos";
	public static final String ENTITY = "IvaTipoEntity";
	public static final String PROPERTY_ID = "Id";
	public static final String PROPERTY_DESCRIPCION = "Descripcion";

	@EdmProperty(name = PROPERTY_ID, nullable = false)
	private Integer id = null;

	@EdmProperty(name = PROPERTY_DESCRIPCION, nullable = false, maxLength = 100)
	private String descripcion = null;

	public IvaTipoEdm() {
		super();
	}

	public IvaTipoEdm(Integer id, String descripcion) {
		super();
		this.id = id;
		this.descripcion = descripcion;
	}

	public IvaTipoEdm(IvaTipoEntity ivaTipoEntity) {
		this(ivaTipoEntity.getId(), ivaTipoEntity.getDescripcion());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		IvaTipoEdm other = (IvaTipoEdm) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "IvaTipoEdm [id=" + id + ", descripcion=" + descripcion + "]";
	}

}
