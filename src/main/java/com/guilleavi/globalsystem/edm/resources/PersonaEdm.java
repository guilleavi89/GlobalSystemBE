package com.guilleavi.globalsystem.edm.resources;

import com.cairone.olingo.ext.jpa.annotations.EdmEntity;
import com.cairone.olingo.ext.jpa.annotations.EdmEntitySet;
import com.cairone.olingo.ext.jpa.annotations.EdmNavigationProperty;
import com.cairone.olingo.ext.jpa.annotations.EdmProperty;
import com.cairone.olingo.ext.jpa.annotations.ODataJPAEntity;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.entities.PersonaEntity;

@EdmEntity(name = PersonaEdm.ENTITY_NAME, key = {
		PersonaEdm.PROPERTY_ID }, namespace = AppConstants.NAME_SPACE, containerName = AppConstants.CONTAINER_NAME)
@EdmEntitySet(PersonaEdm.ENTITY_SET)
@ODataJPAEntity(PersonaEdm.ENTITY)
public class PersonaEdm {

	public static final String ENTITY_NAME = "Persona";
	public static final String ENTITY_SET = "Personas";
	public static final String ENTITY = "PersonaEntity";
	public static final String PROPERTY_ID = "Id";
	public static final String PROPERTY_NOMBRE = "Nombre";
	public static final String PROPERTY_TIPO = "Tipo";
	public static final String PROPERTY_DOCUMENTO_TIPO = "DocumentoTipo";
	public static final String PROPERTY_DOCUMENTO_NUMERO = "DocumentoNumero";

	@EdmProperty(name = PROPERTY_ID, nullable = false)
	private Integer id = null;

	@EdmProperty(name = PROPERTY_NOMBRE, nullable = false, maxLength = 200)
	private String nombre = null;

	@EdmNavigationProperty(name = PROPERTY_TIPO, nullable = false)
	private PersonaTipoEdm tipo = null;

	@EdmNavigationProperty(name = PROPERTY_DOCUMENTO_TIPO, nullable = true)
	private DocumentoTipoEdm documentoTipo = null;

	@EdmProperty(name = PROPERTY_DOCUMENTO_NUMERO, nullable = true)
	private Long documentoNumero = null;

	public PersonaEdm() {
		super();
	}

	public PersonaEdm(Integer id, String nombre, PersonaTipoEdm tipo, DocumentoTipoEdm documentoTipo,
			Long documentoNumero) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.tipo = tipo;
		this.documentoTipo = documentoTipo;
		this.documentoNumero = documentoNumero;
	}

	public PersonaEdm(PersonaEntity personaEntity) {
		this(personaEntity.getId(), personaEntity.getNombre(), new PersonaTipoEdm(personaEntity.getTipo()),
				new DocumentoTipoEdm(personaEntity.getDocumentoTipo()), personaEntity.getDocumentoNumero());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public PersonaTipoEdm getTipo() {
		return tipo;
	}

	public void setTipo(PersonaTipoEdm tipo) {
		this.tipo = tipo;
	}

	public DocumentoTipoEdm getDocumentoTipo() {
		return documentoTipo;
	}

	public void setDocumentoTipo(DocumentoTipoEdm documentoTipo) {
		this.documentoTipo = documentoTipo;
	}

	public Long getDocumentoNumero() {
		return documentoNumero;
	}

	public void setDocumentoNumero(Long documentoNumero) {
		this.documentoNumero = documentoNumero;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PersonaEdm other = (PersonaEdm) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PersonaEdm [id=" + id + ", nombre=" + nombre + "]";
	}

}
