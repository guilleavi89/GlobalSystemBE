package com.guilleavi.globalsystem.edm.resources;

import com.cairone.olingo.ext.jpa.annotations.EdmEntity;
import com.cairone.olingo.ext.jpa.annotations.EdmEntitySet;
import com.cairone.olingo.ext.jpa.annotations.EdmNavigationProperty;
import com.cairone.olingo.ext.jpa.annotations.EdmProperty;
import com.cairone.olingo.ext.jpa.annotations.ODataJPAEntity;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.entities.DomicilioEntity;

@EdmEntity(name = DomicilioEdm.ENTITY_NAME, key = {
		DomicilioEdm.PROPERTY_ID }, namespace = AppConstants.NAME_SPACE, containerName = AppConstants.CONTAINER_NAME)
@EdmEntitySet(DomicilioEdm.ENTITY_SET)
@ODataJPAEntity(DomicilioEdm.ENTITY)
public class DomicilioEdm {

	public static final String ENTITY_NAME = "Domicilio";
	public static final String ENTITY_SET = "Domicilios";
	public static final String ENTITY = "DomicilioEntity";
	public static final String PROPERTY_ID = "Id";
	public static final String PROPERTY_PERSONA = "Persona";
	public static final String PROPERTY_CALLE = "Calle";
	public static final String PROPERTY_NRO = "Nro";
	public static final String PROPERTY_PISO = "Piso";
	public static final String PROPERTY_DPTO = "Dpto";
	public static final String PROPERTY_CIUDAD = "Ciudad";
	public static final String PROPERTY_DESCRIPCION = "Descripcion";
	public static final String PROPERTY_PRINCIPAL = "Principal";

	@EdmProperty(name = PROPERTY_ID, nullable = false)
	private Integer id = null;

	@EdmNavigationProperty(name = PROPERTY_PERSONA, nullable = false)
	private PersonaEdm persona = null;

	@EdmProperty(name = PROPERTY_CALLE, nullable = false, maxLength = 100)
	private String calle = null;

	@EdmProperty(name = PROPERTY_NRO, nullable = false, maxLength = 10)
	private String nro = null;

	@EdmProperty(name = PROPERTY_PISO, nullable = true, maxLength = 5)
	private String piso = null;

	@EdmProperty(name = PROPERTY_DPTO, nullable = true, maxLength = 5)
	private String dpto = null;

	@EdmNavigationProperty(name = PROPERTY_CIUDAD, nullable = false)
	private CiudadEdm ciudad = null;

	@EdmProperty(name = PROPERTY_DESCRIPCION, nullable = true, maxLength = 45)
	private String descripcion = null;

	@EdmProperty(name = PROPERTY_PRINCIPAL, nullable = false)
	private Boolean principal = null;

	public DomicilioEdm() {
		super();
	}

	public DomicilioEdm(Integer id, PersonaEdm persona, String calle, String nro, String piso, String dpto,
			CiudadEdm ciudad, String descripcion, Boolean principal) {
		super();
		this.id = id;
		this.persona = persona;
		this.calle = calle;
		this.nro = nro;
		this.piso = piso;
		this.dpto = dpto;
		this.ciudad = ciudad;
		this.descripcion = descripcion;
		this.principal = principal;
	}

	public DomicilioEdm(DomicilioEntity domicilioEntity) {
		this(domicilioEntity.getId(), new PersonaEdm(domicilioEntity.getPersona()), domicilioEntity.getCalle(),
				domicilioEntity.getNro(), domicilioEntity.getPiso(), domicilioEntity.getDpto(),
				new CiudadEdm(domicilioEntity.getCiudad()), domicilioEntity.getDescripcion(),
				domicilioEntity.getPrincipal());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public PersonaEdm getPersona() {
		return persona;
	}

	public void setPersona(PersonaEdm persona) {
		this.persona = persona;
	}

	public String getCalle() {
		return calle;
	}

	public void setCalle(String calle) {
		this.calle = calle;
	}

	public String getNro() {
		return nro;
	}

	public void setNro(String nro) {
		this.nro = nro;
	}

	public String getPiso() {
		return piso;
	}

	public void setPiso(String piso) {
		this.piso = piso;
	}

	public String getDpto() {
		return dpto;
	}

	public void setDpto(String dpto) {
		this.dpto = dpto;
	}

	public CiudadEdm getCiudad() {
		return ciudad;
	}

	public void setCiudad(CiudadEdm ciudad) {
		this.ciudad = ciudad;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public Boolean getPrincipal() {
		return principal;
	}

	public void setPrincipal(Boolean principal) {
		this.principal = principal;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DomicilioEdm other = (DomicilioEdm) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "DomicilioEdm [id=" + id + "]";
	}
}
