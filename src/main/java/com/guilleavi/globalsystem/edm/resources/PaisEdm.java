package com.guilleavi.globalsystem.edm.resources;

import com.cairone.olingo.ext.jpa.annotations.EdmEntity;
import com.cairone.olingo.ext.jpa.annotations.EdmEntitySet;
import com.cairone.olingo.ext.jpa.annotations.EdmProperty;
import com.cairone.olingo.ext.jpa.annotations.ODataJPAEntity;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.entities.PaisEntity;

@EdmEntity(name = PaisEdm.ENTITY_NAME, key = {
		PaisEdm.PROPERTY_ID }, namespace = AppConstants.NAME_SPACE, containerName = AppConstants.CONTAINER_NAME)
@EdmEntitySet(PaisEdm.ENTITY_SET)
@ODataJPAEntity(PaisEdm.ENTITY)
public class PaisEdm {

	public static final String ENTITY_NAME = "Pais";
	public static final String ENTITY_SET = "Paises";
	public static final String ENTITY = "PaisEntity";
	public static final String PROPERTY_ID = "Id";
	public static final String PROPERTY_NOMBRE = "Nombre";
	public static final String PROPERTY_PREFIJO = "PrefijoTelefonico";

	@EdmProperty(name = PROPERTY_ID, nullable = false)
	private Integer id = null;

	@EdmProperty(name = PROPERTY_NOMBRE, nullable = false, maxLength = 100)
	private String nombre = null;

	@EdmProperty(name = PROPERTY_PREFIJO, nullable = true, maxLength = 5)
	private String prefijoTelefonico = null;

	public PaisEdm() {
		super();
	}

	public PaisEdm(Integer id, String nombre, String prefijoTelefonico) {
		super();
		this.id = id;
		this.nombre = nombre;
		this.prefijoTelefonico = prefijoTelefonico;
	}

	public PaisEdm(PaisEntity paisEntity) {
		this(paisEntity.getId(), paisEntity.getNombre(), paisEntity.getPrefijoTelefonico());
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getPrefijoTelefonico() {
		return prefijoTelefonico;
	}

	public void setPrefijoTelefonico(String prefijoTelefonico) {
		this.prefijoTelefonico = prefijoTelefonico;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		PaisEdm other = (PaisEdm) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "PaisEdm [id=" + id + ", nombre=" + nombre + "]";
	}

}
