package com.guilleavi.globalsystem.edm.resources;

import com.cairone.olingo.ext.jpa.annotations.EdmEntity;
import com.cairone.olingo.ext.jpa.annotations.EdmEntitySet;
import com.cairone.olingo.ext.jpa.annotations.EdmProperty;
import com.cairone.olingo.ext.jpa.annotations.ODataJPAEntity;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.entities.DocumentoTipoEntity;

@EdmEntity(
		name = DocumentoTipoEdm.ENTITY_NAME, 
		key = { DocumentoTipoEdm.PROPERTY_ID }, 
		namespace = AppConstants.NAME_SPACE, containerName = AppConstants.CONTAINER_NAME)
@EdmEntitySet(DocumentoTipoEdm.ENTITY_SET)
@ODataJPAEntity(DocumentoTipoEdm.ENTITY)
public class DocumentoTipoEdm {

	public static final String ENTITY_NAME = "DocumentoTipo";
	public static final String ENTITY_SET = "DocumentoTipos";
	public static final String ENTITY = "DocumentoTipoEntity";
	public static final String PROPERTY_ID = "Id";
	public static final String PROPERTY_DESCRIPCION = "Descripcion";
	public static final String PROPERTY_DESCRIPCION_REDUCIDA = "DescripcionReducida";

	@EdmProperty(name = PROPERTY_ID, nullable = false)
	private Integer id = null;
	
	@EdmProperty(name = PROPERTY_DESCRIPCION, nullable = false, maxLength = 100)
	private String descripcion = null;
	
	@EdmProperty(name = PROPERTY_DESCRIPCION_REDUCIDA, nullable = false, maxLength = 10)
	private String descripcionReducida = null;

	public DocumentoTipoEdm() {
		super();
	}

	public DocumentoTipoEdm(Integer id, String descripcion, String descripcionReducida) {
		super();
		this.id = id;
		this.descripcion = descripcion;
		this.descripcionReducida = descripcionReducida;
	}
	
	public DocumentoTipoEdm(DocumentoTipoEntity documentoTipoEntity) {
		this(
				documentoTipoEntity.getId(),
				documentoTipoEntity.getDescripcion(),
				documentoTipoEntity.getDescripcionReducida()
		);
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getDescripcionReducida() {
		return descripcionReducida;
	}

	public void setDescripcionReducida(String descripcionReducida) {
		this.descripcionReducida = descripcionReducida;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		DocumentoTipoEdm other = (DocumentoTipoEdm) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "DocumentoTipoEdm [id=" + id + ", descripcion=" + descripcion + "]";
	}
}
