package com.guilleavi.globalsystem.edm.operations;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.olingo.commons.api.ex.ODataException;
import org.apache.olingo.server.api.uri.UriParameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.cairone.olingo.ext.jpa.annotations.EdmFunction;
import com.cairone.olingo.ext.jpa.annotations.EdmParameter;
import com.cairone.olingo.ext.jpa.annotations.EdmReturnType;
import com.cairone.olingo.ext.jpa.interfaces.Operation;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.edm.resources.ClienteEdm;
import com.guilleavi.globalsystem.entities.ClienteEntity;
import com.guilleavi.globalsystem.services.ClienteService;

@Component
@EdmFunction(namespace = AppConstants.NAME_SPACE, name = "GetClienteByKeywordFunction", isBound = false)
@EdmReturnType(type = ClienteEdm.EDM_NAME)
public class GetClienteByKeywordFunction implements Operation<ClienteEdm> {

	@Autowired
	ClienteService clienteService = null;

	@EdmParameter(name = "q", nullable = false, maxLength = 200)
	private String keyword = null;

	@Override
	public ClienteEdm doOperation(boolean isBound, Map<String, UriParameter> keyPredicateMap) throws ODataException {

		List<ClienteEntity> clientesEntity = clienteService.findByKeyword(keyword);
		List<ClienteEdm> clientesEdm = new ArrayList<ClienteEdm>();

		for (ClienteEntity clienteEntity : clientesEntity) {

			if (clienteEntity != null) {
//				clientesEdm.add(new ClienteEdm(clienteEntity));
				return new ClienteEdm(clienteEntity);
			}
		}

//		if (clientesEdm != null && !clientesEdm.isEmpty()) {
//			ClienteEdm[] result = clientesEdm.stream().toArray(ClienteEdm[]::new);
//			
//			return result;
//		}
		
		return null;
	}
}
