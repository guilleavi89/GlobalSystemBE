package com.guilleavi.globalsystem.edm.operations;

import com.cairone.olingo.ext.jpa.annotations.EdmFunctionImport;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.edm.resources.ClienteEdm;

@EdmFunctionImport(namespace = AppConstants.NAME_SPACE, name = "GetClienteByKeyword", 
	function = "GetClienteByKeywordFunction", entitySet = ClienteEdm.ENTITY_SET)
public class GetClienteByKeywordFunctionImport {

}
