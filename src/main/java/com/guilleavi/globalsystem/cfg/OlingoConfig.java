package com.guilleavi.globalsystem.cfg;

import org.apache.olingo.server.api.ODataApplicationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.embedded.ServletRegistrationBean;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import com.cairone.olingo.ext.jpa.processors.ActionProcessor;
import com.cairone.olingo.ext.jpa.processors.BatchRequestProcessor;
import com.cairone.olingo.ext.jpa.processors.MediaProcessor;
import com.cairone.olingo.ext.jpa.providers.EdmProvider;
import com.guilleavi.globalsystem.AppConstants;
import com.guilleavi.globalsystem.ctrls.ODataController;

@Configuration
public class OlingoConfig {

	@Value("${globalsystem.odata.maxtopoption}")
	private Integer maxTopOption = null;

	@Autowired
	private ApplicationContext context = null;
	
	@Autowired
	ODataController dispatcherServlet = null;

	@Bean
	public MediaProcessor getMediaProcessor() throws ODataApplicationException {

		MediaProcessor mediaProcessor = new MediaProcessor().setDefaultEdmPackage(AppConstants.DEFAULT_EDM_PACKAGE)
				.setServiceRoot(AppConstants.SERVICE_ROOT).setMaxTopOption(maxTopOption).initialize(context);

		return mediaProcessor;
	}

	@Bean
	public ActionProcessor getActionProcessor() throws ODataApplicationException {

		ActionProcessor processor = new ActionProcessor().setDefaultEdmPackage(AppConstants.DEFAULT_EDM_PACKAGE)
				.setServiceRoot(AppConstants.SERVICE_ROOT).initialize(context);

		return processor;
	}

	@Bean
	public BatchRequestProcessor getBatchRequestProcessor() {
		BatchRequestProcessor processor = new BatchRequestProcessor();
		return processor;
	}

	@Bean
	public EdmProvider getOdataexampleEdmProvider() throws ODataApplicationException {

		EdmProvider provider = new EdmProvider().setContainerName(AppConstants.CONTAINER_NAME)
				.setDefaultEdmPackage(AppConstants.DEFAULT_EDM_PACKAGE).setNameSpace(AppConstants.NAME_SPACE)
				.setServiceRoot(AppConstants.SERVICE_ROOT).initialize();

		return provider;
	}

	@Bean
	public ServletRegistrationBean dispatcherServletRegistration() {
		ServletRegistrationBean registration = new ServletRegistrationBean(dispatcherServlet,
				"/odata/globalsystem.svc/*");
		return registration;
	}
}
